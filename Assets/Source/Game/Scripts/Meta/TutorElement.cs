﻿using System;
using UnityEngine;

namespace WoodFighter
{
    [Serializable]
    public class TutorElement
    {
        [SerializeField] private GameObject _tutorObject;
        [SerializeField] private int _level;

        public GameObject TutorObject => _tutorObject;
        public int Level => _level;

        public string Key => $"{_level}:{_tutorObject.name}";
    }
}
