﻿using Agava.YandexGames;
using System;
using System.Collections;
using UnityEngine;

namespace WoodFighter
{
    public class SdkInitializer : MonoBehaviour
    {
        public event Action Initialized;

        private IEnumerator Start()
        {
#if !UNITY_WEBGL || UNITY_EDITOR
            yield break;
#endif
            yield return YandexGamesSdk.Initialize();
            Initialized?.Invoke();
        }
    }
}
