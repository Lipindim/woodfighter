using System;
using UnityEngine;
using WoodFighter.Models;

namespace WoodFighter
{
    public class SaveLoader : MonoBehaviour
    {
        private const string TotalScore = nameof(TotalScore);
        private const string CurrentScore = nameof(CurrentScore);
        private const string Level = nameof(Level);

        private Score _score;
        private Level _level;

        public void Initialize(Score score,
            Level level)
        {
            _score = score;
            _score.Total.Changed += OnTotalScoreChanged;
            _score.Current.Changed += OnCurrentScoreChanged;
            _level = level;
            _level.Completed += OnLevelCompleted;
            _level.Started += OnLevelStarted;
            _level.GameFinished += OnGameFinished;
        }

        private void OnGameFinished()
        {
            PlayerPrefs.SetInt(Level, 0);
            PlayerPrefs.Save();
        }

        private void OnLevelCompleted()
        {
            PlayerPrefs.SetInt(Level, _level.Current);
            PlayerPrefs.Save();
        }

        private void OnLevelStarted()
        {
            PlayerPrefs.SetInt(Level, _level.Current);
            PlayerPrefs.Save();
        }

        private void OnTotalScoreChanged(int totalScore)
        {
            PlayerPrefs.SetInt(TotalScore, totalScore);
            PlayerPrefs.Save();
        }

        private void OnCurrentScoreChanged(int currentScore)
        {
            PlayerPrefs.SetInt(CurrentScore, currentScore);
            PlayerPrefs.Save();
        }

        public int GetTotalScore()
        {
            return PlayerPrefs.GetInt(TotalScore);
        }

        public int GetCurrentScore()
        {
            return PlayerPrefs.GetInt(CurrentScore);
        }

        public int GetLevel()
        {
            return PlayerPrefs.GetInt(Level);
        }

        public void SetValue(string key, int value)
        {
            PlayerPrefs.SetInt(key, value);
            PlayerPrefs.Save();
        }

        public void SetValue(string key, bool value)
        {
            PlayerPrefs.SetInt(key, value ? 1 : 0);
            PlayerPrefs.Save();
        }

        public bool GetBoolValue(string key)
        {
            if (!PlayerPrefs.HasKey(key))
                return false;

            int value = PlayerPrefs.GetInt(key, 0);
            return value == 1;
        }

        public int GetValue(string key)
        {
            return PlayerPrefs.GetInt(key, 0);
        }
    }
}
