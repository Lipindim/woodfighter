﻿using System;
using WoodFighter.Models;

namespace WoodFighter
{
    public class ScoreSaver
    {
        private LeaderboardConnector _leaderboardConnector;
        private Score _scores;

        public ScoreSaver(LeaderboardConnector leaderboardConnector, Score score)
        {
            _leaderboardConnector = leaderboardConnector;
            _scores = score;
        }

        public event Action ScoreSaved;

        public void SaveResult()
        {
            _leaderboardConnector.DataReceived += OnDataReceived;
            _leaderboardConnector.ReceiveAccountData();
        }

        private void OnDataReceived()
        {
            _leaderboardConnector.ScoreApplyed += OnScoreApplyed;
            _leaderboardConnector.SetScore(_scores.Total.Current);
        }

        private void OnScoreApplyed()
        {
            _leaderboardConnector.ScoreApplyed -= OnScoreApplyed;
            ScoreSaved?.Invoke();
        }
    }
}
