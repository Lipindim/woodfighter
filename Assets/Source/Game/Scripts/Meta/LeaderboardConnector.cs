﻿using Agava.YandexGames;
using Lean.Localization;
using System;
using UnityEngine;

namespace WoodFighter
{
    public class LeaderboardConnector : MonoBehaviour
    {
        private const string LeaderboardName = "Woodfighter";

        private string _name;
        private int _score;
        private int _bestScore;
        private bool _profileDataReceived;
        private bool _playerEntryReceived;

        public event Action<LeaderboardEntryResponse[]> ResultsGot;
        public event Action ScoreApplyed;
        public event Action DataReceived;

        public void SetScore(int score)
        {
            if (score < _bestScore)
            {
                ScoreApplyed?.Invoke();
                return;
            }

            _bestScore = score;
            _score = score;
            Leaderboard.SetScore(LeaderboardName, _score, OnSetScore, extraData: _name);
        }

        public void ReceiveAccountData()
        {
            _profileDataReceived = false;
            _playerEntryReceived = false;
            PlayerAccount.RequestPersonalProfileDataPermission();
            PlayerAccount.GetProfileData(OnGetProileData, OnGetProfileDataFailed);
            Leaderboard.GetPlayerEntry(LeaderboardName, OnGetPlayerEntrySuccess);
        }

        private void OnGetPlayerEntrySuccess(LeaderboardEntryResponse response)
        {
            if (response == null)
                return;
            _bestScore = response.score;
            _playerEntryReceived = true;
            if (_profileDataReceived)
                DataReceived?.Invoke();
        }

        private void OnGetProfileDataFailed(string message)
        {
            Debug.LogError($"Failed: {message}");
            _profileDataReceived = true;
            if (_playerEntryReceived)
                DataReceived?.Invoke();
        }


        private void OnSetScore()
        {
            ScoreApplyed?.Invoke();
        }

        public void StartGetScores()
        {
            Leaderboard.GetEntries(LeaderboardName, OnGetScore);
        }

        private void OnGetProileData(PlayerAccountProfileDataResponse response)
        {
            _name = response.publicName;
            if (string.IsNullOrWhiteSpace(_name))
                _name = TranslateConstants.Anonymous;
            _profileDataReceived = true;
            if (_playerEntryReceived)
                DataReceived?.Invoke();
        }

        private void OnGetScore(LeaderboardGetEntriesResponse response)
        {
            ResultsGot?.Invoke(response.entries);
        }
    }
}
