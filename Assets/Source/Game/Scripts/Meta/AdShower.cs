using Agava.YandexGames;
using System;
using UnityEngine;
using WoodFighter.Models;

namespace WoodFighter
{
    public class AdShower : MonoBehaviour
    {
        private ScoreCounter _scoreCounter;

        public event Action Started;
        public event Action Finished;

        public void Initialize(ScoreCounter scoreCounter)
        {
            _scoreCounter = scoreCounter;
        }

        public void ShowAd()
        {
#if !UNITY_WEBGL || UNITY_EDITOR
            return;
#endif
            Started?.Invoke();
            InterstitialAd.Show(onCloseCallback: OnAdClose);
        }

        private void OnAdClose(bool obj)
        {
            Finished?.Invoke();
        }

        public void DoubleReward()
        {
            VideoAd.Show(onRewardedCallback: OnRewarded, onCloseCallback: OnAdClose);
            Started?.Invoke();
        }

        private void OnAdClose()
        {
            Finished?.Invoke();
        }

        private void OnRewarded()
        {
            _scoreCounter.DoubleScore();
        }
    }
}
