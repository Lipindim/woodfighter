﻿using Agava.YandexGames;
using Lean.Localization;
using TMPro;
using UnityEngine;

namespace WoodFighter
{
    public class LanguageInitializer : MonoBehaviour
    {
        private const string English = "English";
        private const string Russian = "Russian";
        private const string Turkish = "Turkish";
        private const string EnglishCode = "en";
        private const string RussianCode = "ru";
        private const string TurkishCode = "tr";


        [SerializeField] private SdkInitializer _sdkInitializer;
        [SerializeField] private LeanLocalization _leanLanguage;

        private void Awake()
        {
            _sdkInitializer.Initialized += OnSdkInitialized;
        }

        private void OnSdkInitialized()
        {
            string languageCode = YandexGamesSdk.Environment.i18n.lang;
            switch(languageCode)
            {
                case EnglishCode:
                    _leanLanguage.SetCurrentLanguage(English);
                    break;
                case RussianCode:
                    _leanLanguage.SetCurrentLanguage(Russian);
                    break;
                case TurkishCode:
                    _leanLanguage.SetCurrentLanguage(Turkish);
                    break;
            }
        }
    }
}
