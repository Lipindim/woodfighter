using System;
using UnityEngine;
using UnityEngine.UI;
using WoodFighter.Models;

namespace WoodFighter
{
    public class TutorActivator : MonoBehaviour
    {
        [SerializeField] private TutorElement[] _levelStartedElements;
        [SerializeField] private TutorElement[] _levelCompletedElements;
        [SerializeField] private GameObject _tutorObject;
        [SerializeField] private Button _closeButton;
        [SerializeField] private SaveLoader _saveLoader;

        private Level _level;

        public event Action Showed;
        public event Action Hided;

        public void Initialize(Level level)
        {
            _level = level;
            level.Started += OnLevelStarted;
            level.Completed += OnLevelCompleted;
            _closeButton.onClick.AddListener(ContinueGame);
        }

        private void OnLevelCompleted()
        {
            HideElements();
            ActivateElements(_levelCompletedElements);
        }

        private void OnLevelStarted()
        {
            HideElements();
            ActivateElements(_levelStartedElements);
        }

        private void ActivateElements(TutorElement[] tutorElements)
        {
            foreach (var tutorElement in tutorElements)
            {
                if (_level.Current == tutorElement.Level)
                {
                    if (!_saveLoader.GetBoolValue(tutorElement.Key))
                    {
                        _tutorObject.Show();
                        tutorElement.TutorObject.Show();
                        Showed?.Invoke();
                        _saveLoader.SetValue(tutorElement.Key, true);
                    }
                }
            }
        }

        private void HideElements()
        {
            HideElements(_levelStartedElements);
            HideElements(_levelCompletedElements);
        }

        private void HideElements(TutorElement[] elements)
        {
            foreach (var element in elements)
                element.TutorObject.Hide();
        }

        private void ContinueGame()
        {
            _tutorObject.Hide();
            Hided?.Invoke();
        }
    }
}
