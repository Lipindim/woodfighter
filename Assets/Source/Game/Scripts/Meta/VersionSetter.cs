using TMPro;
using UnityEngine;

namespace WoodFighter
{
    public class VersionSetter : MonoBehaviour
    {
        [SerializeField] private TMP_Text _version;

        private void Start()
        {
            _version.SetText(Application.version);
        }
    }
}
