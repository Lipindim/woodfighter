﻿using System;
using UnityEngine;

namespace WoodFighter
{
    public class SwitchMenuPauseInitiator : MonoBehaviour, ILevelPauseInitiator
    {
        [SerializeField] private UiSwitcher _uiSwitcher;

        public event Action<IPauseInitiator> PauseRequest;
        public event Action<IPauseInitiator> ContinueRequest;

        private void OnEnable()
        {
            _uiSwitcher.Switched += OnUiSwitched;
        }

        private void OnDisable()
        {
            _uiSwitcher.Switched -= OnUiSwitched;
        }

        private void OnUiSwitched(UiMenuType currentMenu, UiMenuType previousMenu)
        {
            if (currentMenu == UiMenuType.Game && previousMenu == UiMenuType.Menu)
                ContinueRequest?.Invoke(this);
            if (currentMenu == UiMenuType.Menu && previousMenu == UiMenuType.Game)
                PauseRequest?.Invoke(this);
        }
    }
}