﻿using System;
using UnityEngine;

namespace WoodFighter
{
    public class FocusPauseInitiator : MonoBehaviour, IPauseInitiator
    {
        private const float Delay = 1.0f;
        
        private bool _active;

        public event Action<IPauseInitiator> PauseRequest;
        public event Action<IPauseInitiator> ContinueRequest;

        private void Start()
        {
            Invoke(nameof(Activate), Delay);
        }

        private void Activate()
        {
            _active = true;
        }

        private void OnApplicationFocus(bool focus)
        {
            if (!_active)
                return;

            if (focus)
                ContinueRequest?.Invoke(this);
            else
                PauseRequest?.Invoke(this);
        }

        private void OnApplicationPause(bool pause)
        {
            if (!_active)
                return;

            if (pause)
                PauseRequest?.Invoke(this);
            else
                ContinueRequest?.Invoke(this);
        }
    }
}