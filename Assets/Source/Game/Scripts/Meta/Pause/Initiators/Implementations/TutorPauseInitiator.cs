﻿using System;
using UnityEngine;

namespace WoodFighter
{
    public class TutorPauseInitiator : MonoBehaviour, ILevelPauseInitiator
    {
        [SerializeField] private TutorActivator _tutorActivator;

        public event Action<IPauseInitiator> PauseRequest;
        public event Action<IPauseInitiator> ContinueRequest;

        private void OnEnable()
        {
            _tutorActivator.Showed += OnTutorShowed;
            _tutorActivator.Hided += OnTutorHided;
        }

        private void OnDisable()
        {
            _tutorActivator.Showed += OnTutorShowed;
            _tutorActivator.Hided += OnTutorHided;
        }

        private void OnTutorShowed()
        {
            PauseRequest?.Invoke(this);
        }

        private void OnTutorHided()
        {
            ContinueRequest?.Invoke(this);
        }
    }
}