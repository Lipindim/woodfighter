﻿using System;
using UnityEngine;

namespace WoodFighter
{
    public abstract class AdPauseInitiator : MonoBehaviour, IPauseInitiator
    {
        [SerializeField] private AdShower _adShower;

        public event Action<IPauseInitiator> PauseRequest;
        public event Action<IPauseInitiator> ContinueRequest;

        private void OnEnable()
        {
            _adShower.Started += OnAdStarted;
            _adShower.Finished += OnAddFinished;
        }

        private void OnDisable()
        {
            _adShower.Started -= OnAdStarted;
            _adShower.Finished -= OnAddFinished;
        }

        private void OnAdStarted()
        {
            PauseRequest?.Invoke(this);
        }

        private void OnAddFinished()
        {
            ContinueRequest?.Invoke(this);
        }
    }
}