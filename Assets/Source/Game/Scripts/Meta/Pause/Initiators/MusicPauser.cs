﻿namespace WoodFighter
{
    public class MusicPauser : Pauser<IMusicPauseInitiator>
    {
        protected override void Continue()
        {
            SoundPlayer.Continue();
        }

        protected override void Pause()
        {
            SoundPlayer.Pause();
        }
    }
}
