﻿using WoodFighter.Models;

namespace WoodFighter
{
    public class LevelPauser : Pauser<ILevelPauseInitiator>
    {
        private Level _level;

        public void Initialize(Level level)
        {
            _level = level;
        }

        protected override void Continue()
        {
            _level.Continue();
        }

        protected override void Pause()
        {
            _level.Pause();
        }
    }
}
