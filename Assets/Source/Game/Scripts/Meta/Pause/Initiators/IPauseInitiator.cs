﻿using System;

namespace WoodFighter
{
    public interface IPauseInitiator
    {
        public event Action<IPauseInitiator> PauseRequest;
        public event Action<IPauseInitiator> ContinueRequest;
    }
}
