﻿using System.Collections.Generic;
using UnityEngine;
using WoodFighter;

public abstract class Pauser<T> : MonoBehaviour where T : IPauseInitiator
{
    private T[] _pauseInitiators;
    private List<IPauseInitiator> _activePauseInitiators = new();

    private void Start()
    {
        _pauseInitiators = GetComponentsInChildren<T>();

        foreach (IPauseInitiator pauseInitiator in _pauseInitiators)
        {
            pauseInitiator.PauseRequest += OnPauseRequest;
            pauseInitiator.ContinueRequest += OnContinueRequest;
        }
    }

    private void OnDestroy()
    {
        foreach (IPauseInitiator pauseInitiator in _pauseInitiators)
        {
            pauseInitiator.PauseRequest -= OnPauseRequest;
            pauseInitiator.ContinueRequest -= OnContinueRequest;
        }
    }

    protected abstract void Continue();
    protected abstract void Pause();

    private void OnContinueRequest(IPauseInitiator initiator)
    {
        _activePauseInitiators.Remove(initiator);
        if (_activePauseInitiators.Count == 0)
            Continue();
    }

    private void OnPauseRequest(IPauseInitiator initiator)
    {
        Pause();
        _activePauseInitiators.Add(initiator);
    }

}
