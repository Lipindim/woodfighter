﻿using UnityEngine;

namespace WoodFighter
{
    public static class MonovehaviourExtensions
    {
        public static void Hide(this MonoBehaviour monoBehaviour)
        {
            monoBehaviour.gameObject.SetActive(false);
        }
        public static void Show(this MonoBehaviour monoBehaviour)
        {
            monoBehaviour.gameObject.SetActive(true);
        }

        public static void Hide(this GameObject gameObject)
        {
            gameObject.SetActive(false);
        }
        public static void Show(this GameObject gameObject)
        {
            gameObject.SetActive(true);
        }
    }
}
