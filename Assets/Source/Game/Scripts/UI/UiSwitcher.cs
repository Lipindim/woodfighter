﻿using System;
using System.Linq;
using UnityEngine;

namespace WoodFighter
{
    public class UiSwitcher : MonoBehaviour
    {
        public const float StandardDisableTime = 0.5f;

        [SerializeField] private BaseMenu[] _menus;
        [SerializeField] private UiMenuType _startMenu;
        [SerializeField] private GameObject _uiDisabler;

        private UiMenuType _lastMenu = UiMenuType.None;

        public event SwitchMenuDelegate Switched;
        private void Start()
        {
            Switch(_startMenu);
            DisableUI(StandardDisableTime);
        }

        public void Switch(UiMenuType menuType)
        {
            foreach (var menu in _menus) 
                menu.gameObject.SetActive(menu.Type == menuType);
            if (!_menus.Any(x => x.Type == menuType))
                throw new InvalidOperationException($"Can't find menu {menuType}");
            Switched?.Invoke(menuType, _lastMenu);
            _lastMenu = menuType;
        }

        public void DisableUI(float duration)
        {
            _uiDisabler.SetActive(true);
            Invoke(nameof(EnableUI), duration);
        }

        public void EnableUI()
        {
            _uiDisabler.SetActive(false);
        }
    }

    public delegate void SwitchMenuDelegate(UiMenuType currentMenu, UiMenuType previousMenu);
}
