using UnityEngine;
using UnityEngine.UI;
using WoodFighter;

namespace WoodFighter
{
    [RequireComponent(typeof(Button))]
    public class UiSwitchActivator : MonoBehaviour
    {
        [SerializeField] private UiSwitcher _uiSwitcher;
        [SerializeField] private UiMenuType _menuToSwitch;

        private void Awake()
        {
            GetComponent<Button>().onClick.AddListener(() => _uiSwitcher.Switch(_menuToSwitch));
        }
    }
}
