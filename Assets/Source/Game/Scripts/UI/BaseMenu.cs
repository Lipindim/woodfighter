﻿using UnityEngine;

namespace WoodFighter
{
    public class BaseMenu : MonoBehaviour
    {
        [SerializeField] private UiMenuType _menuType;
        public UiMenuType Type => _menuType;
    }
}
