﻿using UnityEngine;
using UnityEngine.UI;
using WoodFighter;
using WoodFighter.Models;
using WoodFighter.Modules;

public class CompleteLevelShower : MonoBehaviour
{
    [SerializeField] private Button _completeLevel;
    [SerializeField] private float _delay = 0.5f;
    
    private Level _level;
    private Value _score;

    public void Initialize(Level level, Value score)
    {
        _level = level;
        _score = score;
        _completeLevel.onClick.AddListener(CompleteLevel);
        _level.Started += OnLevelStarted;
        _score.Changed += OnScoreChanged;
    }

    private void OnScoreChanged(int score)
    {
        if (_level.Settings.CountToCompleted <= score)
        {
            _completeLevel.enabled = false;
            _completeLevel.Show();
            Invoke(nameof(EnableCompleteLevel), _delay);
        }
        else
        {
            _completeLevel.Hide();
        }
    }

    private void OnLevelStarted()
    {
        _completeLevel.Hide();
    }

    private void CompleteLevel()
    {
        _level.CompleteLevel();
    }

    private void EnableCompleteLevel()
    {
        _completeLevel.enabled = true;
    }
}
