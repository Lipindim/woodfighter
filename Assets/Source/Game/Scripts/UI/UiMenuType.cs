﻿namespace WoodFighter
{
    public enum UiMenuType
    {
        None = 0,
        Menu = 1,
        Game = 2,
        FailResults = 3,
        SuccessResults = 4,
        Leaderboard = 5,
        Upgrade = 6,
        Finish = 7,
        Settings = 8
    }
}
