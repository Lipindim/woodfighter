﻿using UnityEngine;
using WoodFighter.Models;

namespace WoodFighter
{
    public class LowFuelIndicator : MonoBehaviour
    {
        private const string Blink = nameof(Blink);

        [SerializeField] private Animator _fuelAnimator;
        [SerializeField] private float _lowFuelPercent;

        private Player _player;
        private bool _activated = true;

        public void SetPlayer(Player player)
        {
            _player = player;
            _fuelAnimator.SetBool(Blink, false);
            _activated = false;
        }

        private void Update()
        {
            if (_activated)
                return;

            if (_player.FuelPercentages <= _lowFuelPercent)
            {
                _activated = true;
                _fuelAnimator.SetBool(Blink, true);
                SoundPlayer.Play(SoundType.LowEnergy);
            }
        }
    }
}