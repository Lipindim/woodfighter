using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;

public enum SoundType
{
    None = 0,
    Die = 1,
    Fail = 2,
    Victory = 3,
    Menu = 4,
    Game = 5,
    UpgradeCollected = 6,
    UpgradeSelected = 7,
    PlayerDie = 8,
    LowEnergy = 9,
    SuccessBuy = 10,
    FailedBuy = 11,
    Protection = 12
}

public class SoundPlayer : MonoBehaviour
{
    private const string Volume = "Volume";

    private static SoundPlayer _instance;

    [SerializeField] private AudioMixer _mainMixer;
    [SerializeField] private List<Sound> sounds;

    public static bool IsMute => AudioListener.volume == 0;

    private void Awake()
    {
        if (_instance != null) 
            Destroy(_instance.gameObject);

        _instance = this;

        InitSounds();
        LoadVolume();
    }


    private void InitSounds()
    {
        foreach (Sound sound in sounds)
        {
            var newas = gameObject.AddComponent<AudioSource>();
            newas.outputAudioMixerGroup = sound.mixer;
            newas.clip = sound.audioClip;
            newas.loop = sound.isLoop;
            newas.pitch = sound.pitch;
            newas.volume = sound.volume;
            sound.audioSource = newas;
        }
    }

    private void LoadVolume()
    {
        int volume = PlayerPrefs.GetInt(Volume, 1);
        AudioListener.volume = volume;
    }

    public static void Play(SoundType type)
    {
        var sound = _instance.sounds.SingleOrDefault(s => s.type == type);
        if (null == sound)
            return;

        sound.audioSource.Stop();
        sound.audioSource.Play();
        sound.audioSource.pitch = sound.pitch;
    }

    public static void Stop(SoundType type)
    {
        var sound = _instance.sounds.SingleOrDefault(s => s.type == type);
        if (null == sound)
            return;

        sound.audioSource.Stop();
    }

    public static void Pause()
    {
        AudioListener.pause = true;
    }

    public static void Continue()
    {
        AudioListener.pause = false;
    }

    public static void EnableSound()
    {
        AudioListener.volume = 1;
        PlayerPrefs.SetInt(Volume, 1);
    }
    public static void DisableSound()
    {
        AudioListener.volume = 0;
        PlayerPrefs.SetInt(Volume, 0);
    }
}

[Serializable]
public class Sound
{
    public SoundType type;
    public AudioMixerGroup mixer;
    public AudioClip audioClip;
    public AudioSource audioSource;
    public float volume, pitch;
    public bool isLoop;
    public float pitchRange;
}
