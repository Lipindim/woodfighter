﻿using Lean.Localization;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using WoodFighter.Models;

namespace WoodFighter
{
    public class UpgradeView : MonoBehaviour
    {
        [SerializeField] private Button _select;
        [SerializeField] private Image _icon;
        [SerializeField] private Image[] _levels;
        [SerializeField] private TMP_Text _price;
        [SerializeField] private TMP_Text _description;
        
        private Upgrade _upgrade;

        public event Action<Upgrade> Upgraded;

        public void Initialize(Upgrade upgrade)
        {
            _upgrade = upgrade;
            _select.onClick.AddListener(OnSelected);
            ShowUpgrade(upgrade);
        }

        private void ShowUpgrade(Upgrade upgrade)
        {
            _icon.sprite = upgrade.Settings.Icon;
            for (int i = 0; i < upgrade.UpgradeLevel; i++)
                _levels[i].Show();
            for (int i = upgrade.UpgradeLevel; i < _levels.Length; i++)
                _levels[i].Hide();
            _price.SetText(upgrade.NextPrice.ToString());
            string translation = LeanLocalization.GetTranslationText(upgrade.Settings.Description);
            _description.SetText(translation);
        }

        private void OnSelected()
        {
            Upgraded?.Invoke(_upgrade);
            ShowUpgrade(_upgrade);
        }

        private void OnDestroy()
        {
            _select.onClick.RemoveAllListeners();
        }
    }
}
