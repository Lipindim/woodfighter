﻿using UnityEngine;
using WoodFighter.Models;

namespace WoodFighter
{
    public class UpgradeLoader : MonoBehaviour
    {
        [SerializeField] private UpgradeActivator _upgradeActivator;
        [SerializeField] private SaveLoader _saveLoader;

        private Upgrades _upgrades;

        public void Initialize(Upgrades upgrades)
        {
            _upgrades = upgrades;
            _upgrades.Reseted += UpdateUpgrades;
            _upgradeActivator.Upgraded += OnUpgraded;
        }

        private void OnUpgraded(Upgrade upgrade)
        {
            UpdateUpgrade(upgrade);
        }

        public void LoadUpgrades(Upgrades upgrades) 
        {
            foreach (var upgrade in _upgrades)
            {
                upgrade.Reset();
                int level = _saveLoader.GetValue(upgrade.Settings.Name);
                for (int i = 0; i < level; i++)
                    upgrade.IncreaseLevel();
            }
        }

        private void UpdateUpgrades()
        {
            foreach (var upgrade in _upgrades)
                UpdateUpgrade(upgrade);
        }

        private void UpdateUpgrade(Upgrade upgrade)
        {
            _saveLoader.SetValue(upgrade.Settings.Name, upgrade.UpgradeLevel);
        }
    }
}