﻿using System;
using UnityEngine;
using WoodFighter.Models;
using WoodFighter.Modules;

namespace WoodFighter
{
    public class UpgradeActivator : MonoBehaviour
    {
        [SerializeField] private UpgradesView _upgradesView;
        
        private Value _balance;

        public event Action<Upgrade> Upgraded;

        public void Initialize(Value balance)
        {
            _balance = balance;
            _upgradesView.Upgraded += OnUpgraded;
        }

        private void OnUpgraded(Upgrade upgrade)
        {
            if (_balance.Current >= upgrade.NextPrice)
            {
                _balance.Decrease(upgrade.NextPrice);
                upgrade.IncreaseLevel();
                SoundPlayer.Play(SoundType.SuccessBuy);
                Upgraded?.Invoke(upgrade);
            }
            else
            {
                SoundPlayer.Play(SoundType.FailedBuy);
            }
        }
    }
}
