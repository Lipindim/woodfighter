﻿using System;
using UnityEngine;

namespace WoodFighter
{
    [Serializable]
    public class UpgradeLevelSettings
    {
        [SerializeField] private int _level;
        [SerializeField] private Vector3 _position;

        public int Level => _level;
        public Vector3 Position => _position;
    }
}
