﻿using System;
using UnityEngine;

namespace WoodFighter
{
    public class UpgradeObject : MonoBehaviour
    {
        public event Action Collected;

        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out BladeView bladeView))
                Collected.Invoke();
        }
    }
}
