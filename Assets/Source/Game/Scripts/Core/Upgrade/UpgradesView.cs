﻿using System;
using System.Collections.Generic;
using UnityEngine;
using WoodFighter.Models;

namespace WoodFighter
{
    public class UpgradesView : MonoBehaviour
    {
        [SerializeField] private UpgradeView _upgradeViewPrefab;
        [SerializeField] private Transform _container;

        private List<UpgradeView> _upgradeViews = new();

        public event Action<Upgrade> Upgraded;
        private Upgrades _upgrades;

        public void Initialize(Upgrades upgrades)
        {
            _upgrades = upgrades;
        }

        private void OnEnable()
        {
            IEnumerable<Upgrade> upgrades = _upgrades.GetRandomUpgrades(3);
            ShowUpgrades(upgrades);
        }

        public void ShowUpgrades(IEnumerable<Upgrade> upgrades)
        {
            Clear();

            foreach (var upgrade in upgrades)
            {
                var upgradeView = Instantiate(_upgradeViewPrefab, _container);
                _upgradeViews.Add(upgradeView);
                upgradeView.Initialize(upgrade);
                upgradeView.Upgraded += Upgrade;
            } 
        }

        private void Upgrade(Upgrade upgrade)
        {
            Upgraded?.Invoke(upgrade);
        }

        private void Clear()
        {
            foreach (var upgradeView in _upgradeViews)
            {
                upgradeView.Upgraded -= Upgrade;
                Destroy(upgradeView.gameObject);
            }
            _upgradeViews.Clear();
        }
    }
}
