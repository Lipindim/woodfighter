﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using WoodFighter.Models;

namespace WoodFighter
{
    public class UpgradeGameView : MonoBehaviour
    {
        [SerializeField] private Image _icon;
        [SerializeField] private Image[] _levels;


        public void Initialize(Upgrade upgrade)
        {
            ShowUpgrade(upgrade);
        }

        private void ShowUpgrade(Upgrade upgrade)
        {
            _icon.sprite = upgrade.Settings.Icon;
            for (int i = 0; i < upgrade.UpgradeLevel; i++)
                _levels[i].Show();
            for (int i = upgrade.UpgradeLevel; i < _levels.Length; i++)
                _levels[i].Hide();
        }
    }
}
