﻿using UnityEngine;
using WoodFighter.Models;

namespace WoodFighter
{
    public class UpgradesGameView : MonoBehaviour
    {
        [SerializeField] private UpgradeGameView[] _upgradeViews;

        private Upgrades _upgrades;

        public void Initialize(Upgrades upgrades)
        {
            _upgrades = upgrades;
        }

        private void OnEnable()
        {
            if (_upgrades == null)
                return;
            ShowUpgrades();
        }

        public void ShowUpgrades()
        {
            int index = 0;
            foreach (var upgrade in _upgrades)
            {
                if (upgrade.UpgradeLevel != 0)
                {
                    _upgradeViews[index].Initialize(upgrade);
                    _upgradeViews[index].Show();
                    index++;
                }
            }
            for (int i = index; i < _upgradeViews.Length; i++)
                _upgradeViews[i].Hide();
        }
    }
}
