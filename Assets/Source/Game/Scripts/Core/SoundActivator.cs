using UnityEngine;
using WoodFighter.Models;

namespace WoodFighter
{
    public class SoundActivator : MonoBehaviour
    {
        [SerializeField] private UpgradeActivator _upgradeActivator;

        public void Initialize(EnemyStorage enemyStorage, Level level)
        {
            enemyStorage.EnemyDead += OnEnemyDead;
            level.Failed += OnLevelFailed;
            level.Completed += OnLevelCompleted;
            level.Started += OnLevelStarted;
            //_upgradeActivator.UpgradeCollected += () => SoundPlayer.Play(SoundType.UpgradeCollected);
            //_upgradeActivator.UpgradeSelected += (u) => SoundPlayer.Play(SoundType.UpgradeSelected);
        }

        private void Start()
        {
            SoundPlayer.Play(SoundType.Menu);
        }

        private void OnLevelStarted()
        {
            SoundPlayer.Play(SoundType.Game);
            SoundPlayer.Stop(SoundType.Menu);
        }

        private void OnLevelCompleted()
        {
            SoundPlayer.Play(SoundType.Victory);
            SoundPlayer.Play(SoundType.Menu);
            SoundPlayer.Stop(SoundType.Game);
        }

        private void OnLevelFailed()
        {
            SoundPlayer.Play(SoundType.Fail);
            SoundPlayer.Play(SoundType.Menu);
            SoundPlayer.Stop(SoundType.Game);
        }

        private void OnEnemyDead(Enemy enemy)
        {
            if (!enemy.SelfDestracted)
                SoundPlayer.Play(SoundType.Die);
        }
    }
}
