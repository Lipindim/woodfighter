﻿using UnityEngine;

namespace WoodFighter
{
    public class LocalDestroyed : MonoBehaviour
    {
        [SerializeField] private float _livingTime;

        private void Start()
        {
            Destroy(gameObject, _livingTime);
        }
    }
}
