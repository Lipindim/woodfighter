﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using WoodFighter.Views;

namespace WoodFighter
{
    public class Pool : MonoBehaviour
    {
        private Dictionary<string, List<EnemyView>> _pools = new Dictionary<string, List<EnemyView>>();

        public EnemyView Get(EnemyView enemyView)
        {
            string key = enemyView.name;
            if (!_pools.ContainsKey(key))
                return InstantiateEnemyView(enemyView);
            if (_pools[key].Count == 0)
                return Instantiate(enemyView);

            EnemyView freeEnemyView = _pools[key].First();
            freeEnemyView.gameObject.SetActive(true);
            _pools[key].Remove(freeEnemyView);
            return freeEnemyView;
        }

        public void Return(EnemyView enemyView)
        {
            string key = enemyView.name;
            if (!_pools.ContainsKey(key))
                _pools[key] = new List<EnemyView>();

            enemyView.gameObject.SetActive(false);
            enemyView.transform.parent = transform;
            _pools[key].Add(enemyView);
        }

        private EnemyView InstantiateEnemyView(EnemyView enemyView)
        {
            EnemyView newEnemyView = Instantiate(enemyView);
            newEnemyView.name = enemyView.name;
            return newEnemyView;
        }
    }
}