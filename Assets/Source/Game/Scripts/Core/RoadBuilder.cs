﻿using System.Collections.Generic;
using UnityEngine;

namespace WoodFighter
{
    public class RoadBuilder : MonoBehaviour
    {
        [SerializeField] private GameObject _arrow;
        [SerializeField] private GameObject _target;
        [SerializeField] private float _pointDistance;
        [SerializeField] private Transform _parrent;

        private List<GameObject> _spawned = new();

        public void Build(Vector3 target)
        {
            Clear();

            int count = (int) (target.magnitude / _pointDistance);
            for (int i = 0; i < count - 1; i++)
            {
                GameObject arrow = Instantiate(_arrow, _parrent);
                arrow.transform.position = target / count * (i + 1);
                arrow.transform.LookAt(target);
                _spawned.Add(arrow);
            }
            GameObject targetObject = Instantiate(_target, _parrent);
            targetObject.transform.position = target;
            _spawned.Add(targetObject);
        }

        private void Clear()
        {
            foreach (var arrow in _spawned)
                Destroy(arrow);
            _spawned.Clear();
        }
    }
}
