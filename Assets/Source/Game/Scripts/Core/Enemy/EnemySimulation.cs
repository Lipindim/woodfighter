using Assets.Source.Game.Scripts.Core;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using WoodFighter.Models;
using WoodFighter.Views;

namespace WoodFighter
{
    public class EnemySimulation : MonoBehaviour
    {
        [SerializeField] private EnemyViewFactory _enemyViewFactory;
        [SerializeField] private Transform _enemiesParrent;
        [SerializeField] private EnemyDeathEffect _deathEffect;
        [SerializeField] private Pool _pool;
        [SerializeField] private ObjectPool _objectPool;
        [SerializeField] private float _effectLiveTime;
        
        private List<EnemyView> _enemyViews = new();

        public void Initialize(BaseEnemySpawner enemySpawner)
        {
            enemySpawner.Spawned += OnEnemySpawned;
        }

        public void Clear()
        {
            foreach (EnemyView enemyView in _enemyViews)
                Destroy(enemyView.gameObject);
            _enemyViews.Clear();
        }

        private void OnEnemySpawned(Enemy enemy)
        {
            EnemyView enemyViewPrefab = _enemyViewFactory.GetPrefab(enemy);
            EnemyView enemyView = _pool.Get(enemyViewPrefab);
            enemyView.transform.parent = _enemiesParrent;
            enemyView.Initialize(enemy);
            _enemyViews.Add(enemyView);
            enemy.Died += OnEnemyDied;
        }

        private void OnEnemyDied(Enemy enemy)
        {
            enemy.Died -= OnEnemyDied;
            EnemyView enemyView = _enemyViews.First(x => x.Enemy == enemy);
            _pool.Return(enemyView);
            EnemyDeathEffect deathEffect = _objectPool.Get(_deathEffect);
            deathEffect.transform.position = enemyView.transform.position;
            _objectPool.DelayReturn(deathEffect, _effectLiveTime);
            _enemyViews.Remove(enemyView);
        }
    }
}
