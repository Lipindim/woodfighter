﻿using UnityEngine;

namespace WoodFighter
{
    public class StartSizeChanger : MonoBehaviour
    {
        [SerializeField] private float _minSize;
        [SerializeField] private float _maxSize;

        private void Start()
        {
            float randomSize = Random.Range(_minSize, _maxSize);
            transform.localScale = new Vector3(randomSize, randomSize, randomSize);
        }
    }
}