﻿using UnityEngine;

namespace WoodFighter
{
    public class StartRandomRotator : MonoBehaviour
    {
        [SerializeField] private float _maxAngle;

        private void Start()
        {
            float randomAngle = (Random.value - 0.5f) * 2 * _maxAngle;
            transform.rotation = Quaternion.Euler(0, randomAngle, 0);
        }
    }
}