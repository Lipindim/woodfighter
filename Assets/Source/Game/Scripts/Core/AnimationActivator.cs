﻿using UnityEngine;

namespace WoodFighter
{
    public class AnimationActivator : MonoBehaviour
    {
        private const string Jump = nameof(Jump);

        [SerializeField] private Animator _animator;

        private bool _animationActive;
        private Vector3 _previousPosition;

        private void Start()
        {
            _previousPosition = transform.position;
        }

        private void Update()
        {
            if (_previousPosition != transform.position && !_animationActive) 
            {
                _animator.SetBool(Jump, true);
                _animationActive = true;
            }
            if (_previousPosition == transform.position && _animationActive) 
            {
                _animator.SetBool(Jump, false);
                _animationActive = false;
            }
            _previousPosition = transform.position;
        }
    }
}