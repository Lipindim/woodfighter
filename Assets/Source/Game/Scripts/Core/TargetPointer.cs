using DG.Tweening;
using UnityEngine;
using WoodFighter.Models;

namespace WoodFighter
{
    public class TargetPointer : MonoBehaviour
    {
        [SerializeField] private RectTransform _arrow;

        private ITransformable _player;
        private EnemyStorage _enemyStorage;

        public void Initialize(EnemyStorage enemyStorage)
        {
            _enemyStorage = enemyStorage;
        }

        public void SetPlayer(ITransformable player)
        {
            _player = player;
        }

        private void Update()
        {
            if (_player == null)
                return;

            Vector3 closestEnemyPosition = GetClosestEnemy();
            SettingCompas(closestEnemyPosition);
        }

        private Vector3 GetClosestEnemy()
        {
            float minDistance = float.MaxValue;
            Vector3 closestEnemyPosition = Vector3.zero;

            foreach (var enemy in _enemyStorage.Enemies)
            {
                if ((_player.Position - enemy.Position).sqrMagnitude < minDistance)
                {
                    minDistance = (_player.Position - enemy.Position).sqrMagnitude;
                    closestEnemyPosition = enemy.Position;
                }
            }

            return closestEnemyPosition;
        }

        private void SettingCompas(Vector3 closestEnemyPosition)
        {
            float angle = Mathf.Atan2(_player.Position.x - closestEnemyPosition.x, closestEnemyPosition.z - _player.Position.z) * Mathf.Rad2Deg;
            _arrow.DORotate(new Vector3(0, 0, angle), 0.05f);
            //_arrow.rotation = Quaternion.Euler(0, 0, angle);
        }
    }
}
