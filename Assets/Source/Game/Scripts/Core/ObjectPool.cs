﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Source.Game.Scripts.Core
{
    public class ObjectPool : MonoBehaviour
    {
        private Dictionary<string, List<MonoBehaviour>> _pools = new Dictionary<string, List<MonoBehaviour>>();

        public T Get<T>(T gameObject) where T : MonoBehaviour
        {
            string key = gameObject.name;
            if (!_pools.ContainsKey(key))
                return InstantiateObject(gameObject);
            if (_pools[key].Count == 0)
                return InstantiateObject(gameObject);

            T freeGameObject = _pools[key].First().GetComponent<T>();
            freeGameObject.gameObject.SetActive(true);
            _pools[key].Remove(freeGameObject);
            return freeGameObject;
        }

        public void Return(MonoBehaviour gameObject)
        {
            string key = gameObject.name;
            if (!_pools.ContainsKey(key))
                _pools[key] = new List<MonoBehaviour>();

            gameObject.gameObject.SetActive(false);
            gameObject.transform.parent = transform;
            _pools[key].Add(gameObject);
        }

        public void DelayReturn(MonoBehaviour gameObject, float delay)
        {
            StartCoroutine(Return(gameObject, delay));
        }

        private IEnumerator Return(MonoBehaviour gameObject, float delay)
        {
            yield return new WaitForSeconds(delay);
            Return(gameObject);
        }

        private T InstantiateObject<T>(T objectPrefab) where T : MonoBehaviour
        {
            T newObject = Instantiate(objectPrefab);
            newObject.name = objectPrefab.name;
            return newObject;
        }
    }
}