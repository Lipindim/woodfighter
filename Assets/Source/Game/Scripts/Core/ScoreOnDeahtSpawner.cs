﻿using Assets.Source.Game.Scripts.Core;
using UnityEngine;
using WoodFighter.Models;

namespace WoodFighter
{
    public class ScoreOnDeahtSpawner : MonoBehaviour
    {
        [SerializeField] private ScoreOnDeathView _scoreOnDeath;
        [SerializeField] private ObjectPool _objectPool;
        [SerializeField] private Vector3 _offset;
        [SerializeField] private float _destroyDelay;
        
        private EnemyStorage _enemyStorage;
        private Player _player;

        public void Initialize(EnemyStorage enemyStorage)
        {
            _enemyStorage = enemyStorage;
            enemyStorage.EnemyDead += OnEnemyDead;
        }

        private void OnDestroy()
        {
            _enemyStorage.EnemyDead -= OnEnemyDead;
        }

        public void SetPlayer(Player player)
        {
            if (_player != null)
                _player.Died -= OnPlayerDied;
            _player = player;
            _player.Died += OnPlayerDied;
        }

        private void OnPlayerDied(Player player)
        {
            Spawn(player.Position, -ScoreCounter.DiedScorePenalty);
        }

        private void OnEnemyDead(Enemy enemy)
        {
            if (enemy.SelfDestracted)
                return;

            Spawn(enemy.Position, enemy.Tier);
        }

        private void Spawn(Vector3 position, int value)
        {
            ScoreOnDeathView scoreOnDeath = _objectPool.Get(_scoreOnDeath);
            scoreOnDeath.SetValue(value);
            scoreOnDeath.transform.position = position + _offset;
            _objectPool.DelayReturn(scoreOnDeath, _destroyDelay);
        }
    }
}