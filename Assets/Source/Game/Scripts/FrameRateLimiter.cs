﻿using UnityEngine;

namespace WoodFighter
{
    public class FrameRateLimiter : MonoBehaviour
    {
        [SerializeField] private int _frameRate;
        private void Start()
        {
            Application.targetFrameRate = _frameRate;
        }
    }
}