﻿using UnityEngine;

namespace WoodFighter
{
    [CreateAssetMenu(menuName = "Data/TrapSettings")]
    public class TrapSettings : ScriptableObject
    {
        [SerializeField] private int _level;
        [SerializeField] private TrapPosition[] _trapPositions;
        [SerializeField] private Trap[] _randomSpawnTraps;

        public int Level => _level;
        public TrapPosition[] TrapPositions => _trapPositions;
        public Trap[] RandomSpawnTraps => _randomSpawnTraps;
    }
}
