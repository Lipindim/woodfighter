﻿using System;
using UnityEngine;

namespace WoodFighter
{
    [Serializable]
    public class TrapPosition
    {
        [SerializeField] private Trap _trap;
        [SerializeField] private Vector3 _positoin;

        public Trap Trap => _trap;
        public Vector3 Position => _positoin;
    }
}
