﻿using UnityEngine;
using WoodFighter.Views;

namespace WoodFighter.Models
{
    public class Puddle : Trap
    {
        [SerializeField] private float _speedReduceMultiplier;

        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent<PlayerView>(out var playerView))
                playerView.Player.SpeedMultiplier += _speedReduceMultiplier;
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.TryGetComponent<PlayerView>(out var playerView))
                playerView.Player.SpeedMultiplier -= _speedReduceMultiplier;
        }
    }
}