﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using WoodFighter;
using WoodFighter.Models;

namespace Assets.Source.Game.Scripts.Traps
{
    public class TrapSpawner : MonoBehaviour
    {
        [SerializeField] private TrapSettings[] _trapsSettings;
        [SerializeField] private Transform _trapsParrent;
        [SerializeField] private Vector3[] _randomSpawnPlaces;

        private List<Trap> _traps = new();
        private ILevel _level;

        public void Initialize(ILevel level)
        {
            _level = level;
            _level.Started += OnLevelStarted;
            _level.Completed += ClearTraps;
            _level.Failed += ClearTraps;
        }

        private void OnDestroy()
        {
            _level.Started -= OnLevelStarted;
            _level.Completed -= ClearTraps;
            _level.Failed -= ClearTraps;
        }

        private void OnLevelStarted()
        {
            ClearTraps();
            TrapSettings trapSettings = _trapsSettings.FirstOrDefault(x => x.Level == _level.Current);
            if (trapSettings != null)
            {
                SpawnTraps(trapSettings);
                SpawnTrapsInRandomPosition(trapSettings);
            }
        }

        private void SpawnTrapsInRandomPosition(TrapSettings trapSettings)
        {
            List<Vector3> randomSpawnPlaces = _randomSpawnPlaces.ToList();
            foreach (Trap trapPrefab in trapSettings.RandomSpawnTraps) 
            {
                if (randomSpawnPlaces.Count == 0)
                    break;

                Vector3 randomSpawnPlace = randomSpawnPlaces.GetRandomElement();
                Trap trap = Instantiate(trapPrefab, _trapsParrent);
                trap.transform.position = randomSpawnPlace;
                _traps.Add(trap);
                randomSpawnPlaces.Remove(randomSpawnPlace);
            }
        }

        private void ClearTraps()
        {
            foreach (Trap trap in _traps)
                Destroy(trap.gameObject);
            _traps.Clear();
        }

        private void SpawnTraps(TrapSettings trapSettings)
        {
            foreach (TrapPosition trapPosition in trapSettings.TrapPositions)
            {
                Trap trap = Instantiate(trapPosition.Trap, _trapsParrent);
                trap.transform.position = trapPosition.Position;
                _traps.Add(trap);
            }
        }
    }
}