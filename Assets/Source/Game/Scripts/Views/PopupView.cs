﻿using UnityEngine;
using UnityEngine.UI;

namespace WoodFighter
{
    public class PopupView : MonoBehaviour
    {
        [SerializeField] private Button _switch;
        [SerializeField] private GameObject _popup;
        [SerializeField] private float _showingTime = 5.0f;

        private void Start()
        {
            _switch.onClick.AddListener(SwitchPopupState);
        }

        private void OnDestroy()
        {
            _switch.onClick.RemoveAllListeners();
        }

        private void SwitchPopupState()
        {
            if (_popup.activeSelf)
            {
                _popup.Hide();
            }
            else
            {
                _popup.Show();
                Invoke(nameof(HidePopup), _showingTime);
            }
        }

        private void HidePopup()
        {
            _popup.Hide();
        }
    }
}