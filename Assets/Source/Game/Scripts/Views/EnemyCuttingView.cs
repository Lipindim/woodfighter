﻿using UnityEngine;

namespace WoodFighter.Views
{
    public class EnemyCuttingView : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out EnemyView enemyView))
                enemyView.Enemy.DealDamage();
        }
    }
}
