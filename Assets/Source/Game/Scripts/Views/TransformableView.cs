using UnityEngine;
using WoodFighter.Models;

public class TransformableView : MonoBehaviour
{
    private ITransformable _transformable;

    public void Initialize(ITransformable transformable)
    {
        _transformable = transformable;
    }

    private void Update()
    {
        if (_transformable == null)
            return;

        transform.position = _transformable.Position;
        transform.rotation = Quaternion.Euler(_transformable.Rotation);
    }
}
