﻿using System;
using System.Net.NetworkInformation;
using UnityEngine;
using UnityEngine.UI;

namespace WoodFighter
{
    public class MuteView : MonoBehaviour
    {
        [SerializeField] private Toggle _muteToggle;

        private void Start()
        {
            _muteToggle.onValueChanged.AddListener(OnValueChanged);
            _muteToggle.isOn = !SoundPlayer.IsMute;
        }

        private void OnValueChanged(bool value)
        {
            if (value)
                SoundPlayer.EnableSound();
            else
                SoundPlayer.DisableSound();
        }
    }
}