﻿using UnityEngine;
using WoodFighter.Models;
using DG.Tweening;

namespace WoodFighter.Views
{
    public class PlayerView : TransformableView
    {
        [SerializeField] private Transform _leftBlade;
        [SerializeField] private Transform _rightBlade;
        [SerializeField] private Vector3 _smallScale;
        [SerializeField] private float _changingScaleDuration = 0.1f;

        public Player Player { get; private set; }

        public void SetPlayer(Player player)
        {
            base.Initialize(player);
            Player = player;
            ChangeRotationAxis();
        }

        public void ChangeRotationAxis()
        {
            if (Player.RotationMultiplier < 0)
            {
                _leftBlade.transform.DOScale(Vector3.one, _changingScaleDuration);
                _rightBlade.transform.DOScale(_smallScale, _changingScaleDuration);
            }
            else
            {
                _leftBlade.transform.DOScale(_smallScale, _changingScaleDuration);
                _rightBlade.transform.DOScale(Vector3.one, _changingScaleDuration);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out EnemyView enemyView))
            {
                enemyView.Enemy.SelfDestraction();
                if (!Player.DealDamage())
                    SoundPlayer.Play(SoundType.Protection);
            }
        }
    }
}