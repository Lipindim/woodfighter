﻿using TMPro;
using UnityEngine;

namespace WoodFighter
{
    public class ScoreOnDeathView : MonoBehaviour
    {
        [SerializeField] private TMP_Text _value;

        public void SetValue(int value)
        {
            if (value > 0)
                _value.SetText($"+{value}");
            else
                _value.SetText(value.ToString());
        }
    }
}