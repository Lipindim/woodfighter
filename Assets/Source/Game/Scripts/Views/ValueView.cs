﻿using System.Collections;
using TMPro;
using UnityEngine;
using WoodFighter.Modules;

namespace WoodFighter.Views
{
    public class ValueView : MonoBehaviour
    {

        [SerializeField] private TMP_Text _value;

        public void Initialize(Value value)
        {
            value.Changed += OnTotalScoreChanged;
        }

        private void OnTotalScoreChanged(int totalScore)
        {
            _value.SetText(totalScore.ToString());
        }
    }
}