﻿using UnityEngine;
using WoodFighter.Views;

namespace WoodFighter
{
    public class BladeView : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out EnemyView enemyView))
                enemyView.Enemy.DealDamage();
        }
    }
}
