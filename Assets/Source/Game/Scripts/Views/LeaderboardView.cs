﻿using Agava.YandexGames;
using UnityEngine;

namespace WoodFighter
{
    public class LeaderboardView : MonoBehaviour
    {
        [SerializeField] private LeaderboardConnector _leaderboardConnector;
        [SerializeField] private LeaderboardResultView[] _leaderboardResultsView;
        [SerializeField] private GameObject _waiting;
        [SerializeField] private GameObject _leaderboard;

        private void OnEnable()
        {
            _leaderboardConnector.ResultsGot += OnResultsGot;
            _leaderboardConnector.StartGetScores();
        }

        private void OnDisable()
        {
            _waiting.SetActive(true);
            _leaderboard.SetActive(false);
        }

        private void OnResultsGot(LeaderboardEntryResponse[] results)
        {
            _leaderboardConnector.ResultsGot -= OnResultsGot;

            for (int i = 0; i < results.Length && i <_leaderboardResultsView.Length; i++)
                _leaderboardResultsView[i].Set(results[i]);
            for (int i = results.Length; i < _leaderboardResultsView.Length; i++)
            {
                _leaderboardResultsView[i].Clear();
                _leaderboardResultsView[i].gameObject.SetActive(false);
            }

            _waiting.SetActive(false);
            _leaderboard.SetActive(true);
        }
    }
}
