﻿using Agava.YandexGames;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace WoodFighter
{
    public class AuthorizationView : MonoBehaviour
    {
        [SerializeField] private Button _authorize;
        [SerializeField] private Button _close;
        [SerializeField] private LeaderboardConnector _leaderboardShower;

        public event Action Completed;

        public void Start()
        {
            _authorize.onClick.AddListener(Authorize);
            _close.onClick.AddListener(CloseAuthorize);
        }

        private void Authorize()
        {
            PlayerAccount.Authorize(OnAuthrizeSuccess);
        }

        private void OnAuthrizeSuccess()
        {
            if (!PlayerAccount.HasPersonalProfileDataPermission)
                PlayerAccount.RequestPersonalProfileDataPermission();
            gameObject.SetActive(false);
            Completed?.Invoke();
        }

        private void CloseAuthorize()
        {
            gameObject.SetActive(false);
            Completed?.Invoke();
        }
    }
}
