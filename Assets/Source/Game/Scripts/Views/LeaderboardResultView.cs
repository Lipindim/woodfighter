﻿using Agava.YandexGames;
using Lean.Localization;
using TMPro;
using UnityEngine;

namespace WoodFighter
{
    public class LeaderboardResultView : MonoBehaviour
    {
        [SerializeField] private TMP_Text _rank;
        [SerializeField] private TMP_Text _name;
        [SerializeField] private TMP_Text _score;

        public void Set(LeaderboardEntryResponse entry)
        {
            _rank.SetText(entry.rank.ToString());
            if (entry.extraData == TranslateConstants.Anonymous)
                _name.SetText(LeanLocalization.GetTranslationText(entry.extraData));
            else
                _name.SetText(entry.extraData);
            _score.SetText(entry.score.ToString());
        }

        public void Clear()
        {
            _rank.SetText(string.Empty);
            _name.SetText(string.Empty);
            _score.SetText(string.Empty);
        }
    }
}
