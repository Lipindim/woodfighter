﻿using System;
using UnityEngine;
using WoodFighter.Models;

namespace WoodFighter.Views
{
    public class EnemyViewFactory : MonoBehaviour
    {
        [SerializeField] private EnemyView _tier1EnemyView;
        [SerializeField] private EnemyView _tier2EnemyView;
        [SerializeField] private EnemyView _tier3EnemyView;

        public EnemyView GetPrefab(Enemy enemy)
        {
            if (enemy is Tier1Enemy)
                return _tier1EnemyView;
            else if (enemy is Tier2Enemy)
                return _tier2EnemyView;
            else if (enemy is Tier3Enemy)
                return _tier3EnemyView;

            throw new InvalidOperationException($"Unknown enemy type {enemy.GetType().Name}");
        }
    }
}
