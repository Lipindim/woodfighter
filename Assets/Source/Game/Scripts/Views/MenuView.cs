﻿using UnityEngine;
using UnityEngine.UI;
using WoodFighter.Models;

namespace WoodFighter
{
    public class MenuView : MonoBehaviour
    {
        [SerializeField] private Button _continueButton;
        [SerializeField] private Button _backButton;
        
        private ILevel _level;

        public void Initialize(ILevel level)
        {
            _level = level;
            ConfigureButtonsVisibility();
        }

        private void ConfigureButtonsVisibility()
        {
            if (_level.Running)
            {
                _continueButton.Hide();
                _backButton.Show();
            }
            else if (_level.Current != 0)
            {
                _continueButton.Show();
                _backButton.Hide();
            }
            else
            {
                _continueButton.Hide();
                _backButton.Hide();
            }
        }

        private void OnEnable()
        {
            ConfigureButtonsVisibility();
        }
    }
}