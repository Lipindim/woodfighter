﻿using UnityEngine;
using WoodFighter.Models;

namespace WoodFighter.Views
{
    public class EnemyView : MonoBehaviour
    {
        private Enemy _enemy;

        public Enemy Enemy => _enemy;

        public void Initialize(Enemy enemy)
        {
            _enemy = enemy;
            Update();
        }

        private void Update()
        {
            transform.position = _enemy.Position;
            transform.rotation = Quaternion.Euler(_enemy.Rotation);
        }
    }
}

