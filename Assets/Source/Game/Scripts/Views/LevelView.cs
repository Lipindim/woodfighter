﻿using TMPro;
using UnityEngine;
using WoodFighter.Models;

namespace WoodFighter
{
    public class LevelView : MonoBehaviour
    {
        [SerializeField] private TMP_Text _round;

        private Level _level;

        public void Initialize(Level level)
        {
            _level = level;
        }

        private void Update()
        {
            _round.SetText(_level.Current.ToString());
        }
    }
}
