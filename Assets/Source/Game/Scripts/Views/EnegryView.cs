using UnityEngine;
using UnityEngine.UI;
using WoodFighter.Models;

namespace WoodFighter.Views
{
    public class EnegryView : MonoBehaviour
    {
        [SerializeField] private Slider _slider;

        private Player _player;

        public void SetPlayer(Player player)
        {
            _player = player;
        }

        private void Update()
        {
            _slider.value = _player.FuelPercentages / 100;
        }
    }
}
