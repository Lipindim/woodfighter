﻿using TMPro;
using UnityEngine;
using WoodFighter.Models;
using WoodFighter.Modules;

namespace WoodFighter
{
    public class ScoreView : MonoBehaviour
    {
        [SerializeField] private TMP_Text _value;
        
        private Value _score;
        private ILevel _level;

        public void Initialize(Value score, ILevel level)
        {
            score.Changed += OnScoreChanged;
            _score = score;
            _level = level;
            SetScore();
        }

        private void OnScoreChanged(int score)
        {
            SetScore();
        }

        private void SetScore()
        {
            _value.SetText($"{_score.Current}/{_level.Settings.CountToCompleted}");
        }
    }
}
