﻿using UnityEngine;
using UnityEngine.UI;

namespace WoodFighter
{
    public class ButtonMuteView : MonoBehaviour
    {
        [SerializeField] private Button _muteButton;
        [SerializeField] private Sprite _muteSprite;
        [SerializeField] private Sprite _unmuteSprite;
        [SerializeField] private Image _muteIcon;

        private void Start()
        {
            if (SoundPlayer.IsMute)
                _muteIcon.sprite = _muteSprite;
            else
                _muteIcon.sprite= _unmuteSprite;

            _muteButton.onClick.AddListener(ChangeMute);
        }

        private void ChangeMute()
        {
            if (SoundPlayer.IsMute)
            {
                SoundPlayer.EnableSound();
                _muteIcon.sprite = _unmuteSprite;
            }
            else
            {
                SoundPlayer.DisableSound();
                _muteIcon.sprite = _muteSprite;
            }
        }
    }
}