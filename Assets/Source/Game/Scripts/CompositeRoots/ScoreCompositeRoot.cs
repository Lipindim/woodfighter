﻿using UnityEngine;
using WoodFighter.Models;
using WoodFighter.Views;

namespace WoodFighter
{
    public class ScoreCompositeRoot : MonoBehaviour
    {
        [SerializeField] private ScoreView[] _scores;
        [SerializeField] private ValueView[] _levelScores;
        [SerializeField] private ValueView[] _totalScores;
        [SerializeField] private ValueView[] _currentScores;

        public void Initialize(Score score, ILevel level)
        {
            foreach (ScoreView scoreView in _scores)
                scoreView.Initialize(score.Level, level);

            foreach (ValueView valueView in _levelScores)
                valueView.Initialize(score.Level);

            foreach (ValueView valueView in _totalScores)
                valueView.Initialize(score.Total);

            foreach(ValueView valueView in _currentScores)
                valueView.Initialize(score.Current);
        }
    }
}