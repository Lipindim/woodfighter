﻿using Agava.YandexGames;
using Assets.Source.Game.Scripts.Traps;
using UnityEngine;
using WoodFighter.Models;

namespace WoodFighter
{
    public class CompositeRoot : MonoBehaviour
    {
        [SerializeField] private PlayerSettings _playerSettings;
        [SerializeField] private LevelsSettings _levelsSettings;
        [SerializeField] private Upgrade[] _upgrades;

        [SerializeField] private UiCompositeRoot _uiCompositeRoot;
        [SerializeField] private UiSwitcher _uiSwitcher;
        [SerializeField] private EnemySimulation _enemySimulation;
        [SerializeField] private AdShower _adShower;
        [SerializeField] private SaveLoader _saveLoader;
        [SerializeField] private LeaderboardConnector _leaderboardConnector;
        [SerializeField] private TutorActivator _tutorActivator;
        [SerializeField] private SdkInitializer _sdkInitializer;
        [SerializeField] private SoundActivator _soundActivator;
        [SerializeField] private LevelPauser _levelPauser;
        [SerializeField] private ScoreOnDeahtSpawner _scoreOnDeahtSpawner;
        [SerializeField] private CompleteLevelShower _completeLevelShower;

        [SerializeField] private AuthorizationView _authorizationView;
        [SerializeField] private UpgradeActivator _upgradeActivator;
        [SerializeField] private UpgradeLoader _upgradeLoader;
        [SerializeField] private TrapSpawner _trapSpawner;
        [SerializeField] private TargetPointer _targetPointer;

        private Level _level;
        private Score _score;
        private ScoreCounter _scoreCounter;
        private ScoreSaver _scoreSaver;
        private EnemyStorage _enemyStorage;

        private void Awake()
        {
            PlayerFactory playerFactory = new (_playerSettings);

            AbstractEnemyFactory enemyFactory = new(new Tier1EnemyFactory(), new Tier2EnemyFactory(), new Tier3EnemyFactory());
            _enemyStorage = new();
            OneTimeSpawner enemySpawner = new(enemyFactory, _enemyStorage);
            EnemyUpdater enemyUpdater = new(enemySpawner);
            var upgrades = new Upgrades(_upgrades);
            _upgradeLoader.Initialize(upgrades);
            _upgradeLoader.LoadUpgrades(upgrades);

            _enemySimulation.Initialize(enemySpawner);
            _score = new(_saveLoader.GetTotalScore(), _saveLoader.GetCurrentScore());
            _level = new Level(playerFactory, enemySpawner, enemyUpdater, _score, _levelsSettings.Value, _saveLoader.GetLevel());
            _scoreCounter = new(_score, _enemyStorage);
            _scoreSaver = new ScoreSaver(_leaderboardConnector, _score);
            _saveLoader.Initialize(_score, _level);
            _soundActivator.Initialize(_enemyStorage, _level);
            _adShower.Initialize(_scoreCounter);
            _tutorActivator.Initialize(_level);
            _scoreOnDeahtSpawner.Initialize(_enemyStorage);
            _upgradeActivator.Initialize(_score.Current);
            _levelPauser.Initialize(_level);
            _trapSpawner.Initialize(_level);
            _targetPointer.Initialize(_enemyStorage);
            _completeLevelShower.Initialize(_level, _score.Level);

            _level.Failed += ShowRestart;
            _level.Completed += ShowResults;
            _level.GameFinished += Finish;

            _uiCompositeRoot.Initialize(_level, _score, _scoreCounter, _enemyStorage, upgrades);
        }

        private void ShowRestart()
        {
            _uiSwitcher.Switch(UiMenuType.FailResults);
            _uiSwitcher.DisableUI(UiSwitcher.StandardDisableTime);
        }

        private void ShowResults()
        {
            _scoreCounter.ApplyLevelScore();
            _uiSwitcher.Switch(UiMenuType.SuccessResults);
            _uiSwitcher.DisableUI(UiSwitcher.StandardDisableTime);
        }

        private void Finish()
        {
            _uiSwitcher.Switch(UiMenuType.Finish);
            _uiSwitcher.DisableUI(1.0f);

            if (PlayerAccount.IsAuthorized)
            {
                _scoreSaver.SaveResult();
            }
            else
            {
                _authorizationView.gameObject.SetActive(true);
                _authorizationView.Completed += OnAuthorizationCompleted;
            }
        }

        private void OnAuthorizationCompleted()
        {
            if (PlayerAccount.IsAuthorized)
                _scoreSaver.SaveResult();
        }

        private void Update()
        {
            _level.Update(Time.deltaTime);
        }
    }
}