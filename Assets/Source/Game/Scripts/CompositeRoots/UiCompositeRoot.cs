﻿using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using WoodFighter.Models;
using WoodFighter.Views;

namespace WoodFighter
{
    public class UiCompositeRoot : MonoBehaviour
    {
        [SerializeField] private MenuCompositeRoot _menuCompositeRoot;
        [SerializeField] private ScoreCompositeRoot _scoreCompositeRoot;

        [SerializeField] private UiSwitcher _uiSwitcher;
        [SerializeField] private EnemySimulation _enemySimulation;
        [SerializeField] private AdShower _adShower;
        [SerializeField] private SaveLoader _saveLoader;
        [SerializeField] private ScoreOnDeahtSpawner _scoreOnDeahtSpawner;
        [SerializeField] private TargetPointer _targetPointer;
        [SerializeField] private LowFuelIndicator _lowFuelIndicator;

        [SerializeField] private PlayerView _playerView;
        [SerializeField] private EnegryView _energyView;
        [SerializeField] private LevelView _levelVeiw;
        [SerializeField] private AuthorizationView _authorizationView;
        [SerializeField] private MenuView _menuView;
        [SerializeField] private UpgradesView _upgradesView;
        [SerializeField] private UpgradesGameView _upgradesGameView;

        [SerializeField] private Button _pauseButton;
        [SerializeField] private Button _restartButton;
        [SerializeField] private Button _nextLevelButton;
        [SerializeField] private Button _doubleScore;
        [SerializeField] private Button[] _backToMenu;

        private Level _level;
        private Score _score;
        private EnemyStorage _enemyStorage;
        private Upgrades _upgrades;
        private Player _player;
        private ScoreCounter _scoreCounter;

        public void Initialize(Level level, Score score, ScoreCounter scoreCounter, EnemyStorage enemyStorage, Upgrades upgrades)
        {
            _level = level;
            _score = score;
            _scoreCounter = scoreCounter;
            _enemyStorage = enemyStorage;
            _upgrades = upgrades;

            _menuCompositeRoot.Initialize(StartGame, _level, _score, _upgrades);
            _scoreCompositeRoot.Initialize(_score, _level);

            _restartButton.onClick.AddListener(ContinueGame);
            _nextLevelButton.onClick.AddListener(NextLevel);
            _doubleScore.onClick.AddListener(DoubleReward);
            _pauseButton.onClick.AddListener(Pause);
            foreach (Button backToMenu in _backToMenu)
                backToMenu.onClick.AddListener(() => _uiSwitcher.Switch(UiMenuType.Menu));
            
            _levelVeiw.Initialize(_level);
            _menuView.Initialize(_level);
            _upgradesView.Initialize(upgrades);
            _upgradesGameView.Initialize(upgrades);
            _level.Completed += () => _doubleScore.interactable = true;
        }

        private void ContinueGame()
        {
            StartGame();
        }

        private void NextLevel()
        {
            StartGame();
            _adShower.ShowAd();
        }

        private void Pause()
        {
            _uiSwitcher.Switch(UiMenuType.Menu);
        }

        private void StartGame()
        {
            _enemySimulation.Clear();
            _enemyStorage.Clear();
            _score.Level.Reset();

            if (_player != null)
                _player.Died -= OnPlayerDied;
            _player = _level.Prepare();
            ActivateUpgrades(_player);
            _player.Died += OnPlayerDied;
            _playerView.SetPlayer(_player);
            _energyView.SetPlayer(_player);
            _scoreCounter.SetPlayer(_player);
            _scoreOnDeahtSpawner.SetPlayer(_player);
            _targetPointer.SetPlayer(_player);
            _lowFuelIndicator.SetPlayer(_player);

            _level.Start();

            _uiSwitcher.Switch(UiMenuType.Game);
        }

        private void ActivateUpgrades(Player player)
        {
            foreach (var upgrade in _upgrades)
                upgrade.Activate(player);
        }

        private void OnPlayerDied(Player player)
        {
            _level.Pause();
            SoundPlayer.Play(SoundType.PlayerDie);
            StartCoroutine(ReturnToStart());
        }

        private IEnumerator ReturnToStart()
        {
            yield return DOTween.To(() => _player.Position, x => _player.SetPosition(x), Vector3.zero, 3).WaitForCompletion();
            _level.Continue();
            _player.Restore();
        }

        private void DoubleReward()
        {
            _doubleScore.interactable = false;
            _adShower.DoubleReward();
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0) 
                && !EventSystem.current.IsPointerOverGameObject()
                && !_level.IsPause)
            {
                if (_player == null)
                    return;
                if (_player.TryChangeRotationAxis())
                    _playerView.ChangeRotationAxis();
            }
        }
    }
}
