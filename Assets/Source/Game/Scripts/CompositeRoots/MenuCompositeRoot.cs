﻿using Agava.YandexGames;
using System;
using UnityEngine;
using UnityEngine.UI;
using WoodFighter.Models;

namespace WoodFighter
{
    public class MenuCompositeRoot : MonoBehaviour
    {
        [SerializeField] private Button _newGameButton;
        [SerializeField] private Button _continueButton;
        [SerializeField] private Button _backButton;
        [SerializeField] private Button _leaderboardButton;
        [SerializeField] private UiSwitcher _uiSwitcher;
        [SerializeField] private AuthorizationView _authorizationView;

        private Action _startGame;
        private Level _level;
        private Score _score;
        private Upgrades _upgrades;

        public void Initialize(Action startGame,
            Level level,
            Score score,
            Upgrades upgrades)
        {
            _startGame = startGame;
            _level = level;
            _score = score;
            _upgrades = upgrades;

            _newGameButton.onClick.AddListener(NewGame);
            _continueButton.onClick.AddListener(() => startGame.Invoke());
            _backButton.onClick.AddListener(() => _uiSwitcher.Switch(UiMenuType.Game));
            _leaderboardButton.onClick.AddListener(ShowLeaderboard);
        }

        private void OnDestroy()
        {
            _newGameButton.onClick.RemoveAllListeners();
            _continueButton.onClick.RemoveAllListeners();
            _backButton.onClick.RemoveAllListeners();
            _leaderboardButton.onClick.RemoveAllListeners();
        }

        private void NewGame()
        {
            _score.Reset();
            _upgrades.Reset();
            _level.Reset();
            _startGame.Invoke();
        }

        private void ShowLeaderboard()
        {
            if (PlayerAccount.IsAuthorized)
                _uiSwitcher.Switch(UiMenuType.Leaderboard);
            else
                _authorizationView.Show();
        }
    }
}