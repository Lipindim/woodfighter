﻿
using System;
using UnityEngine;

namespace WoodFighter.Models
{
    [Serializable]
    public class PlayerSettings
    {
        [SerializeField] private int _maxEnergy;
        [SerializeField] private int _energyConsumption;
        [SerializeField] private float _radius;
        [SerializeField] private float _rotationSpeed;

        public int MaxEnergy => _maxEnergy;
        public int EnergyConsumption => _energyConsumption;
        public float Radius => _radius;
        public float RotationSpeed => _rotationSpeed;
    }
}