﻿namespace WoodFighter.Models
{
    public class PlayerFactory
    {
        private PlayerSettings _settings;

        public PlayerFactory(PlayerSettings settings)
        {
            _settings = settings;
        }

        public Player CreatePlayer()
        {
            return new Player(_settings);
        }
    }
}