﻿
using System;
using UnityEngine;

namespace WoodFighter.Models
{
    public class Player : IUpdateble, ITransformable, IDamageble<Player>
    {
        private readonly PlayerSettings _settings;
        private readonly RotationMovement _rotationMovement;
        private readonly Energy _energy;

        private int _extraLives = 0;

        public Player(PlayerSettings settings) 
        {
            _settings = settings;
            _rotationMovement = new RotationMovement(
                new RotationMovementParameters(
                    startPosition: Vector3.zero,
                    startRotation: Vector3.zero,
                    speed: _settings.RotationSpeed,
                    radius: _settings.Radius));
            _energy = new Energy(_settings.MaxEnergy, _settings.EnergyConsumption);
            _energy.EnergyIsOver += () => EnergyIsOver?.Invoke();
        }

        public event Action EnergyIsOver;
        public event Action<Player> Died;

        public Vector3 Position => _rotationMovement.Position;
        public Vector3 Rotation => _rotationMovement.Rotation;
        public float RotationMultiplier => _rotationMovement.RotationMultiplier;
        public float FuelPercentages => _energy.EnergyPercentages;

        public bool Dead { get; private set; } = false;
        public float SpeedMultiplier
        {
            get
            { return _rotationMovement.SpeedMultiplier; }
            set
            { _rotationMovement.SpeedMultiplier = value; }
        }

        public void Update(float deltaTime)
        {
            _energy.Update(deltaTime);
            _rotationMovement.Update(deltaTime);
        }

        public bool DealDamage()
        {
            if (_extraLives <= 0)
            {
                Dead = true;
                Died?.Invoke(this);
                return true;
            }
            else
            {
                _extraLives--;
                return false;
            }
        }

        public void Restore()
        {
            Dead = false;
        }

        public void UpgradeSpeed(float speedUpgradeValue)
        {
            _rotationMovement.IncreaseSpeed(speedUpgradeValue);
        }

        public void UpgradeEnergy(int upgradeEnergyValue)
        {
            _energy.Upgrade(upgradeEnergyValue);
        }
        public void AddExtraLive()
        {
            _extraLives++;
        }

        public bool TryChangeRotationAxis()
        {
            if (Dead)
                return false;

            return _rotationMovement.ChangeRotationAxis();
        }

        public void SetPosition(Vector3 position)
        {
            _rotationMovement.SetPosition(position);
        }
    }
}