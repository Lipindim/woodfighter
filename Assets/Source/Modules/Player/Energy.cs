﻿using System;

namespace WoodFighter.Models
{
    public class Energy : IUpdateble
    {
        private int _max;
        private float _value;
        private float _consumption;

        public Energy(int maxEnergy, float energyConsumption) 
        {
            _max = maxEnergy;
            _consumption = energyConsumption;
            _value = maxEnergy;
        }

        public event Action EnergyIsOver;

        public float EnergyPercentages => _value / _max * 100;

        public void Update(float deltaTime)
        {
            _value -= _consumption * deltaTime;
            if (_value <= 0)
                EnergyIsOver?.Invoke();
        }

        public void Upgrade(int upgradeValue)
        {
            _max += upgradeValue;
            _value += upgradeValue;
        }
    }
}
