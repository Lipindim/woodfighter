﻿using UnityEngine;

namespace WoodFighter.Models
{
    public class MovementParameters
    {
        public MovementParameters(Vector3 startPosition, Vector3 startRotation, float speed)
        {
            StartPosition = startPosition;
            StartRotation = startRotation;
            Speed = speed;
        }

        public Vector3 StartPosition { get; }
        public Vector3 StartRotation { get; }
        public float Speed { get; }
    }
}
