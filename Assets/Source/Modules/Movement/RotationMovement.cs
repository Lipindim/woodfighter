using UnityEngine;

namespace WoodFighter.Models
{
    public class RotationMovement : ITransformable
    {
        private Vector3 _position;
        private Vector3 _rotationAxisPosition;
        private float _radius;
        private float _rotationSpeed;
        private float _rotationAngle;

        private float _changeRotationAxisDelay;

        public float SpeedMultiplier { get; set; } = 1.0f;
        public float RotationMultiplier { get; private set; }

        public RotationMovement(RotationMovementParameters parameters)
        {
            _position = parameters.StartPosition;
            _rotationAngle = parameters.StartRotation.y;
            _rotationSpeed = parameters.Speed;
            _rotationAxisPosition = _position;
            _radius = parameters.Radius;
            RotationMultiplier = 1;
            CalculateRotationAxisPosition();
        }

        public Vector3 Position => _position;
        public Vector3 Rotation => new Vector3(0, _rotationAngle);

        public bool ChangeRotationAxis()
        {
            if (_changeRotationAxisDelay > 0)
                return false;

            _changeRotationAxisDelay = 0.2f;
            _rotationAxisPosition = _position * 2 - _rotationAxisPosition;
            RotationMultiplier *= -1;
            return true;
        }

        public void Update(float deltaTime)
        {
            if (_changeRotationAxisDelay > 0)
                _changeRotationAxisDelay -= deltaTime;
            _rotationAngle += (deltaTime * _rotationSpeed * RotationMultiplier * SpeedMultiplier) % 360;
            RecalculatePosition();
        }

        public void IncreaseSpeed(float value)
        {
            _rotationSpeed += value;
        }

        public void SetPosition(Vector3 position)
        {
            Vector3 deltaPosition = _position - _rotationAxisPosition;
            _position = position;
            _rotationAxisPosition = _position - deltaPosition;
        }

        private void RecalculatePosition()
        {
            _position = _rotationAxisPosition + new Vector3(_radius * Mathf.Sin(_rotationAngle * Mathf.Deg2Rad),
                0,
                _radius * Mathf.Cos(_rotationAngle * Mathf.Deg2Rad)) * RotationMultiplier;
        }

        private void CalculateRotationAxisPosition()
        {
            _rotationAxisPosition = _position - new Vector3(_radius * Mathf.Sin(_rotationAngle * Mathf.Deg2Rad),
                0,
                _radius * Mathf.Cos(_rotationAngle * Mathf.Deg2Rad)) * RotationMultiplier;
        }
    }
}
