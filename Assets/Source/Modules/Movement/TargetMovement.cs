﻿using UnityEngine;

namespace WoodFighter.Models
{
    public class TargetMovement : ITransformable
    {
        private readonly ITransformable _target;
        private readonly float _speed;
        private Vector3 _position;
        private Vector3 _rotation;

        public TargetMovement(MovementParameters movementParameters,
            ITransformable target)
        {
            _target = target;
            _position = movementParameters.StartPosition;
            _rotation = movementParameters.StartRotation;
            _speed = movementParameters.Speed;
        }

        public Vector3 Position => _position;
        public Vector3 Rotation => _rotation;

        public void Update(float deltaTime)
        {
            Vector3 movingDirection = (_target.Position - _position).normalized;
            _position += movingDirection * deltaTime * _speed;
            float rotationY = Mathf.Atan2(movingDirection.x , movingDirection.z) * Mathf.Rad2Deg;
            _rotation = new Vector3(0, rotationY, 0);
        }
    }
}
