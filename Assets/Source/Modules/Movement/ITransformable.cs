﻿using UnityEngine;

namespace WoodFighter.Models
{
    public interface ITransformable : IUpdateble
    {
        public Vector3 Position { get; }
        public Vector3 Rotation { get; }
    }
}
