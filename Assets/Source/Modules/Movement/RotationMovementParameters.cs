﻿
using UnityEngine;

namespace WoodFighter.Models
{
    public class RotationMovementParameters : MovementParameters
    {
        public RotationMovementParameters(Vector3 startPosition, Vector3 startRotation, float speed, float radius) 
            : base(startPosition, startRotation, speed)
        {
            Radius = radius;
        }

        public float Radius { get;}
    }
}