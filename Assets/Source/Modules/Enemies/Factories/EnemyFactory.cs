﻿using UnityEngine;

namespace WoodFighter.Models
{
    public abstract class EnemyFactory
    {
        private const float MaxRotation = 30.0f;
        protected Vector3 GetRandomAngle()
        {
            return new Vector3(0, (Random.value - 0.5f) * 2 * MaxRotation);
        }
        public abstract bool CanCreate(int enemyTier);
        public abstract Enemy Create(EnemyParameters enemyParameters);
    }
}