﻿using UnityEngine;

namespace WoodFighter.Models
{
    public class Tier3EnemyFactory : EnemyFactory
    {
        private const float Speed = 8.0f;

        public override bool CanCreate(int enemyTier)
        {
            return enemyTier == 3;
        }

        public override Enemy Create(EnemyParameters enemyParameters)
        {
            var movementParameters = new MovementParameters(
                startPosition: enemyParameters.StartPosition,
                startRotation: Vector3.zero,
                speed: Speed);
            //var movement = new TargetMovement(movementParameters, enemyParameters.Target);
            var movement = new NearestOneMovement(movementParameters, enemyParameters.Target, 35);
            return new Tier3Enemy(movement);
        }
    }
}
