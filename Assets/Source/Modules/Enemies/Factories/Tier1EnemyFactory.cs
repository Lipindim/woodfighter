﻿

using UnityEngine;

namespace WoodFighter.Models
{
    public class Tier1EnemyFactory : EnemyFactory
    {
        private const float Speed = 4.0f;

        public override bool CanCreate(int enemyTier)
        {
            return enemyTier == 1;
        }

        public override Enemy Create(EnemyParameters enemyParameters)
        {
            var movementParameters = new MovementParameters(
                startPosition: enemyParameters.StartPosition,
                startRotation: GetRandomAngle(),
                speed: Speed);
            //var movement = new TargetMovement(movementParameters, enemyParameters.Target);
            var movement = new NearestOneMovement(movementParameters, enemyParameters.Target, 25);
            return new Tier1Enemy(movement);
        }
    }
}

