﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace WoodFighter.Models
{
    public class AbstractEnemyFactory
    {
        private IEnumerable<EnemyFactory> _factories;

        public AbstractEnemyFactory(params EnemyFactory[] factories)
        {
            _factories = factories;
        }

        public bool CanCreate(int enemyTier)
        {
            return _factories.Any(f => f.CanCreate(enemyTier));
        }

        public Enemy Create(EnemyParameters enemyParameters)
        {
            EnemyFactory factory = _factories.FirstOrDefault(f => f.CanCreate(enemyParameters.EnemyTier));
            if (factory == null)
                throw new InvalidOperationException("Unknown enemy type.");

            return factory.Create(enemyParameters);
        }
    }
}