﻿using UnityEngine;

namespace WoodFighter.Models
{
    public class Tier2EnemyFactory : EnemyFactory
    {
        private const float Speed = 6.0f;

        public override bool CanCreate(int enemyTier)
        {
            return enemyTier == 2;
        }

        public override Enemy Create(EnemyParameters enemyParameters)
        {
            var movementParameters = new MovementParameters(
                startPosition: enemyParameters.StartPosition,
                startRotation: Vector3.zero,
                speed: Speed);
            //var movement = new TargetMovement(movementParameters, enemyParameters.Target);
            var movement = new NearestOneMovement(movementParameters, enemyParameters.Target, 30);
            return new Tier2Enemy(movement);
        }
    }
}
