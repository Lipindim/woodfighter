﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace WoodFighter.Models
{
    [Serializable]
    public class OneTimeSpawnParameters
    {
        [SerializeField] private int _startCount = 2;
        [SerializeField] private float _multiplier = 1.5f;
        [SerializeField] private float _startRadius = 30.0f;
        [SerializeField] private float _deltaRadius = 12.0f;
        [SerializeField] private int _rounds = 10;
        [SerializeField] private EnemySpawnParameters[] _enemySpawnParameters;

        public int StartCount => _startCount;
        public float Multiplier => _multiplier;
        public float StartRadius => _startRadius;
        public float DeltaRadius => _deltaRadius;
        public int Rounds => _rounds;
        public IReadOnlyList<EnemySpawnParameters> EnemySpawnParameters => _enemySpawnParameters;
    }
}
