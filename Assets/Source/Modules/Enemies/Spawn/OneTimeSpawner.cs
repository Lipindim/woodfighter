﻿using System;
using System.Linq;
using UnityEngine;

namespace WoodFighter.Models
{
    public class OneTimeSpawner : BaseEnemySpawner, IUpdateble
    {
        private readonly EnemyStorage _enemyStorage;
        private readonly AbstractEnemyFactory _enemyFactory;

        private OneTimeSpawnParameters _spawnParameters;
        private ITransformable _target;

        public OneTimeSpawner(AbstractEnemyFactory enemyFactory,
            EnemyStorage enemyStorage)
        {
            _enemyStorage = enemyStorage;
            _enemyFactory = enemyFactory;
        }

        public override event Action<Enemy> Spawned;

        public override void StartSpawn(OneTimeSpawnParameters spawnParameters, ITransformable target)
        {
            _spawnParameters = spawnParameters;
            _target = target;

            Activate();
            Spawn();
        }

        private void Spawn()
        {
            int enemyCount = _spawnParameters.StartCount;
            float radius = _spawnParameters.StartRadius;

            for (int i = 0; i < _spawnParameters.Rounds; i++)
            {
                SpawnEnemyWave(enemyCount, radius);
                enemyCount = (int)(_spawnParameters.Multiplier * enemyCount);
                radius += _spawnParameters.DeltaRadius;
            }
        }

        public override void StopSpawn()
        {
            Deactivate();
        }

        public override void Update(float deltaTime)
        {
            if (!IsActive)
                return;
        }

        private void SpawnEnemyWave(int enemyCount, float radius)
        {
            int[] frequencies = _spawnParameters.EnemySpawnParameters
                .Select(x => x.SpawnFrequency)
                .ToArray();

            float angleOffset = (UnityEngine.Random.value - 1 / 2) * Mathf.PI / 2;
            for (int i = 0; i < enemyCount; i++)
            {
                int randomEnemyIndex = Randomizer.GetRandomIndex(frequencies);
                var enemyParameters = new EnemyParameters(
                    _spawnParameters.EnemySpawnParameters[randomEnemyIndex].EnemyTier,
                    CalculateStartPosition(i, enemyCount, angleOffset, GetRandomRadius(radius)),
                    _target);
                Enemy enemy = _enemyFactory.Create(enemyParameters);
                Spawned?.Invoke(enemy);
                _enemyStorage.Add(enemy);
            }
        }

        private float GetRandomRadius(float radius)
        {
            return radius + _spawnParameters.DeltaRadius / 2 * (UnityEngine.Random.value - 0.5f);
        }

        private Vector3 CalculateStartPosition(int index, int total, float angleOffset, float radius)
        {
            Vector3 center = _target.Position;
            float angle = 2 * Mathf.PI / total * index + angleOffset;
            Vector3 offset = new Vector3(Mathf.Sin(angle), 0, Mathf.Cos(angle)) * radius;
            return center + offset;
        }
    }
}
