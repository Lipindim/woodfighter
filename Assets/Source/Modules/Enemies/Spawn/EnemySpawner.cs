﻿//using System;
//using System.Linq;
//using UnityEngine;

//namespace WoodFighter.Models
//{
//    public class EnemySpawner : BaseEnemySpawner, IUpdateble
//    {
//        private const float SpawnRadius = 30.0f;

//        private readonly EnemyStorage _enemyStorage;
//        private readonly AbstractEnemyFactory _enemyFactory;

//        private Timer _timer;
//        private SpawnParameters _spawnParameters;
//        private ITransformable _target;

//        public EnemySpawner(AbstractEnemyFactory enemyFactory,
//            EnemyStorage enemyStorage)
//        {
//            _enemyStorage = enemyStorage;
//            _enemyFactory = enemyFactory;
//            _timer = new Timer();
//        }

//        public override event Action<Enemy> Spawned;

//        public override void StartSpawn(SpawnParameters spawnParameters, ITransformable target)
//        {
//            _spawnParameters = spawnParameters;
//            _timer.Start(_spawnParameters.CycleDuration);
//            _timer.Tick += SpawnEnemyWave;
//            _target = target;
//            Activate();
//        }

//        public override void StopSpawn()
//        {
//            _timer.Stop();
//            _timer.Tick -= SpawnEnemyWave;
//            Deactivate();
//        }

//        public override void Update(float deltaTime)
//        {
//            if (!IsActive)
//                return;

//            _timer.Update(deltaTime);
//        }

//        private void SpawnEnemyWave()
//        {
//            int spawningEnemies = Randomizer.GetRandom(_spawnParameters.MinCycleEnemies, _spawnParameters.MaxCycleEnemies);
//            int[] frequencies = _spawnParameters.EnemySpawnParameters
//                .Select(x => x.SpawnFrequency)
//                .ToArray();

//            float angleOffset = (UnityEngine.Random.value - 1 / 2) * Mathf.PI / 2;
//            for (int i = 0; i < spawningEnemies; i++)
//            {
//                int randomEnemyIndex = Randomizer.GetRandomIndex(frequencies);
//                var enemyParameters = new EnemyParameters(
//                    _spawnParameters.EnemySpawnParameters[randomEnemyIndex].EnemyTier,
//                    CalculateStartPosition(i, spawningEnemies, angleOffset),
//                    _target);
//                Enemy enemy = _enemyFactory.Create(enemyParameters);
//                Spawned?.Invoke(enemy);
//                _enemyStorage.Add(enemy);
//            }
//        }

//        private Vector3 CalculateStartPosition(int index, int total, float angleOffset)
//        {
//            Vector3 center = _target.Position;
//            float angle = 2 * Mathf.PI / total * index + angleOffset;
//            Vector3 offset = new Vector3(Mathf.Sin(angle), 0, Mathf.Cos(angle)) * SpawnRadius;
//            return center + offset;
//        }
//    }
//}
