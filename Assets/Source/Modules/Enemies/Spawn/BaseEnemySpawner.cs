﻿using System;

namespace WoodFighter.Models
{
    public abstract class BaseEnemySpawner : Activated
    {
        public abstract event Action<Enemy> Spawned;

        public abstract void StartSpawn(OneTimeSpawnParameters spawnParameters, ITransformable target);
        public abstract void StopSpawn();
        public abstract void Update(float deltaTime);
    }
}