﻿
using System;
using System.Collections.Generic;
using UnityEngine;

namespace WoodFighter.Models
{
    [Serializable]
    public class SpawnParameters
    {
        [SerializeField] private float _cycleDuration;
        [SerializeField] private int _minCycleEnemies;
        [SerializeField] private int _maxCycleEnemies;
        [SerializeField] private EnemySpawnParameters[] _enemySpawnParameters;

        public float CycleDuration { get => _cycleDuration; set => _cycleDuration = value; }
        public int MinCycleEnemies { get => _minCycleEnemies; set => _minCycleEnemies = value; }
        public int MaxCycleEnemies { get => _maxCycleEnemies; set => _maxCycleEnemies = value; }
        public IReadOnlyList<EnemySpawnParameters> EnemySpawnParameters => _enemySpawnParameters;
    }
}