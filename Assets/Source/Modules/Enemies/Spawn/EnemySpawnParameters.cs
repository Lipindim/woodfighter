﻿using System;
using UnityEngine;

namespace WoodFighter.Models
{
    [Serializable]
    public class EnemySpawnParameters
    {
        [SerializeField] private int _enemyTier;
        [SerializeField] private int _spawnFrequency;

        public int EnemyTier => _enemyTier;
        public int SpawnFrequency => _spawnFrequency;
    }
}