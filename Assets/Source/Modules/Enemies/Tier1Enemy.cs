﻿
namespace WoodFighter.Models
{
    public class Tier1Enemy : Enemy
    {
        public Tier1Enemy(ITransformable transformable) : base(transformable)
        {
        }

        public override int Tier => 1;
    }
}