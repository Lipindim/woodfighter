﻿
using System;
using System.Collections.Generic;

namespace WoodFighter.Models
{
    public class EnemyStorage
    {
        private readonly List<Enemy> _enemies = new List<Enemy>();
        
        public IEnumerable<Enemy> Enemies => _enemies;

        public event Action<Enemy> EnemyDead;

        public void Add(Enemy enemy)
        {
            enemy.Died += OnEnemyDead;
            _enemies.Add(enemy);
        }

        private void OnEnemyDead(Enemy enemy)
        {
            enemy.Died -= OnEnemyDead;
            EnemyDead?.Invoke(enemy);
            _enemies.Remove(enemy);
        }

        public void Clear()
        {
            foreach (Enemy enemy in _enemies)
                enemy.Died -= OnEnemyDead;
            _enemies.Clear();
        }
    }
}