﻿
namespace WoodFighter.Models
{
    public class Tier3Enemy : Enemy
    {
        public Tier3Enemy(ITransformable transformable) : base(transformable)
        {
        }

        public override int Tier => 3;
    }
}