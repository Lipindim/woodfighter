﻿using System.Collections.Generic;

namespace WoodFighter.Models
{
    public class EnemyUpdater : IUpdateble
    {
        private BaseEnemySpawner _enemySpawner;
        private List<Enemy> _enemies = new List<Enemy>();

        public EnemyUpdater(BaseEnemySpawner enemySpawner)
        {
            _enemySpawner = enemySpawner;
            _enemySpawner.Spawned += OnEnemySpawned;
        }

        private void OnEnemySpawned(Enemy enemy)
        {
            _enemies.Add(enemy);
            enemy.Died += OnEnemyDied;
        }

        private void OnEnemyDied(Enemy enemy)
        {
            enemy.Died -= OnEnemyDied;
            _enemies.Remove(enemy);
        }

        public void Update(float deltaTime)
        {
            foreach (var enemy in _enemies)
                enemy.Update(deltaTime);
        }
    }
}
