﻿
using System;
using UnityEngine;

namespace WoodFighter.Models
{
    public abstract class Enemy : IUpdateble, ITransformable, IDamageble<Enemy>
    {
        private ITransformable _transformable;

        public Enemy(ITransformable transformable)
        {
            _transformable = transformable;
        }

        public bool SelfDestracted { get; private set; }
        public event Action<Enemy> Died;

        public Vector3 Position => _transformable.Position;
        public Vector3 Rotation => _transformable.Rotation;
        public abstract int Tier { get; }

        public bool Dead { get; private set; }

        public void Update(float deltaTime)
        {
            _transformable.Update(deltaTime);
        }

        public bool DealDamage()
        {
            Dead = true;
            Died?.Invoke(this);
            return true;
        }

        public void SelfDestraction()
        {
            SelfDestracted = true;
            Dead = true;
            Died?.Invoke(this);
        }
    }
}