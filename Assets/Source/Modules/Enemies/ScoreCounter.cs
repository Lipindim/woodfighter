﻿using System;

namespace WoodFighter.Models
{
    public class ScoreCounter
    {
        public const int DiedScorePenalty = 5;

        private Score _score;
        private Player _player;

        public ScoreCounter(Score score, EnemyStorage enemyStorage)
        {
            enemyStorage.EnemyDead += OnEnemyDead;
            _score = score;
        }

        public void SetPlayer(Player player)
        {
            if (_player != null)
                _player.Died -= OnPlayerDied;
            _player = player;
            _player.Died += OnPlayerDied;
        }

        private void OnPlayerDied(Player player)
        {
            int value = Math.Min(DiedScorePenalty, _score.Level.Current);
            _score.Level.Decrease(value);
        }

        private void OnEnemyDead(Enemy enemy)
        {
            if (enemy.SelfDestracted)
                return;

            _score.Level.Increase(enemy.Tier);
        }

        public void ApplyLevelScore()
        {
            _score.Total.Increase(_score.Level.Current);
            _score.Current.Increase(_score.Level.Current);
        }

        public void DoubleScore()
        {
            _score.Total.Increase(_score.Level.Current);
            _score.Current.Increase(_score.Level.Current);
            _score.Level.Increase(_score.Level.Current);
        }
    }
}
