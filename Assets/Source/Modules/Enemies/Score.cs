﻿using System;
using WoodFighter.Modules;

namespace WoodFighter.Models
{
    public class Score
    {
        public Value Level { get; private set; }
        public Value Total { get; private set; }
        public Value Current { get; private set; }

        public Score(int totalScore, int currentScore)
        {
            Total = new Value(totalScore);
            Current = new Value(currentScore);
            Level = new Value(totalScore);
        }

        public void Reset()
        {
            Level.Reset();
            Total.Reset();
            Current.Reset();
        }
    }
}
