﻿
namespace WoodFighter.Models
{
    public class Tier2Enemy : Enemy
    {
        public Tier2Enemy(ITransformable transformable) : base(transformable)
        {
        }

        public override int Tier => 2;
    }
}