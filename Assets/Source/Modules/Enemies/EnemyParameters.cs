﻿using UnityEngine;

namespace WoodFighter.Models
{
    public class EnemyParameters
    {
        public EnemyParameters(int enemyTier, 
            Vector3 startPosition, 
            ITransformable target)
        {
            EnemyTier = enemyTier;
            StartPosition = startPosition;
            Target = target;
        }

        public int EnemyTier { get; }
        public Vector3 StartPosition { get; }
        public ITransformable Target { get; }

    }
}
