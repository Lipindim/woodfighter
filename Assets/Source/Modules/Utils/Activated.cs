﻿
namespace WoodFighter.Models
{
    public class Activated
    {
        public bool IsActive { get; private set; }

        protected void Activate()
        {
            IsActive = true;
        }

        protected void Deactivate()
        {
            IsActive = false;
        }
    }
}