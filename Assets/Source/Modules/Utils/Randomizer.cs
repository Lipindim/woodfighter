﻿using System;
using System.Linq;

namespace WoodFighter.Models
{
    public static class Randomizer
    {
        private static Random _random = new Random();
        
        public static int GetRandom(int min, int max)
        {
            return _random.Next(min, max);
        }

        public static int GetRandomIndex(params int[] frequencies) 
        {
            int sum = frequencies.Sum();
            int random = _random.Next(sum + 1);
            int index = 0;
            int value = 0;

            foreach (int frequency in frequencies) 
            { 
                value += frequency;
                if (value >= random)
                    return index;

                index++;
            }

            throw new InvalidOperationException("Random index calculate error");
        }
    }
}
