﻿
using System;

namespace WoodFighter.Models
{
    public class Timer : Activated, IUpdateble
    {
        private float _interval;
        private float _remainingTime;

        public void Start(float interval)
        {
            _interval = interval;
            _remainingTime = _interval;
            Activate();
        }

        public void Stop()
        {
            Deactivate();
        }

        public event Action Tick;

        public void Update(float deltaTime)
        {
            if (!IsActive)
                return;

            _remainingTime -= deltaTime;
            if (_remainingTime <= 0)
            {
                Tick?.Invoke();
                _remainingTime = _interval;
            }
        }
    }
}