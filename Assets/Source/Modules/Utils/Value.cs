﻿using System;

namespace WoodFighter.Modules
{
    public class Value
    {
        public int Current { get; private set; }
        
        public event Action<int> Changed;

        public Value(int initialValue)
        {
            Current = initialValue;
        }

        public void Increase(int value)
        {
            Current += value;
            Changed?.Invoke(Current);
        }

        public void Decrease(int value)
        {
            if (value > Current)
                throw new ArgumentOutOfRangeException("value");

            Current -= value; 
            Changed?.Invoke(Current);
        }

        public void Reset()
        {
            Current = 0;
            Changed?.Invoke(Current);
        }
    }
}
