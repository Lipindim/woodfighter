﻿using System.Collections.Generic;
using UnityEngine;

namespace WoodFighter.Models
{
    [CreateAssetMenu(menuName = "Data/LevelsSettings")]
    public class LevelsSettings : ScriptableObject
    {
        [SerializeField] private LevelSettings[] _levelsSettings;

        public IList<LevelSettings> Value => _levelsSettings;
    }
}
