﻿using UnityEngine;

namespace WoodFighter.Models
{
    [CreateAssetMenu(menuName = "Data/LevelSettings")]
    public class LevelSettings : ScriptableObject
    {
        //[SerializeField] private SpawnParameters _spawnParameters;
        //[SerializeField] private Vector3 _destinationPoint;
        [SerializeField] private int _countToCompleted;
        [SerializeField] private OneTimeSpawnParameters _spawnParameters;
        //public SpawnParameters SpawnParameters => _spawnParameters;
        //public Vector3 DestinationPoint => _destinationPoint;
        public int CountToCompleted => _countToCompleted;
        public OneTimeSpawnParameters SpawnParameters => _spawnParameters;
    }
}