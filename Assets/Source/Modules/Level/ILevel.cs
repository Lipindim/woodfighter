﻿using System;

namespace WoodFighter.Models
{
    public interface ILevel
    {
        public event Action Started;
        public event Action Completed;
        public event Action Failed;
        public event Action GameFinished;
        public event Action NewGameStarted;

        public LevelSettings Settings { get; }
        public Player Player { get; }
        public bool Running { get; }
        public int Current { get; }
    }
}
