﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WoodFighter.Models
{
    public class Level : ILevel, IUpdateble
    {
        private readonly PlayerFactory _playerFactory;
        private readonly BaseEnemySpawner _enemySpawner;
        private readonly LevelSettings[] _levelSettings;
        private readonly EnemyUpdater _enemyUpdater;
        private readonly Score _score;
        
        public Level(PlayerFactory playerFactory,
            BaseEnemySpawner enemySpawner,
            EnemyUpdater enemyUpdater,
            Score score,
            IEnumerable<LevelSettings> levelSettings,
            int startLevel)
        {
            _playerFactory = playerFactory;
            _enemySpawner = enemySpawner;
            _levelSettings = levelSettings.ToArray();
            _enemyUpdater = enemyUpdater;
            _score = score;
            Current = startLevel;
        }

        public event Action NewGameStarted;
        public event Action Started;
        public event Action Completed;
        public event Action Failed;
        public event Action GameFinished;

        public LevelSettings Settings => _levelSettings[Current];
        public Player Player { get; private set; }
        public bool Running { get; private set; }
        public int Current { get; private set; }
        public bool IsPause { get; private set; }

        public Player Prepare()
        {
            Player = _playerFactory.CreatePlayer();
            return Player;
        }

        public void Start()
        {
            _enemySpawner.StartSpawn(Settings.SpawnParameters, Player);
            Player.EnergyIsOver += OnEnergyIsOver;
            Running = true;
            Started?.Invoke();
            if (Current == 0)
                NewGameStarted?.Invoke();
        }

        public void Stop()
        {
            _enemySpawner.StopSpawn();
            Player.EnergyIsOver -= OnEnergyIsOver;
            Running = false;
        }

        public void Update(float deltaTime)
        {
            if (!Running || IsPause)
                return;

            Player.Update(deltaTime);
            _enemySpawner.Update(deltaTime);
            _enemyUpdater.Update(deltaTime);
            //if (HasPlayerReachedDestination())
            //    CompleteLevel();
        }

        public void Pause()
        {
            IsPause = true;
        }

        public void Continue()
        {
            IsPause = false;
        }

        public void Reset()
        {
            Current = 0;
        }

        public void CompleteLevel()
        {
            Stop();
            Current++;
            Completed?.Invoke();
            if (Current >= _levelSettings.Length)
            {
                GameFinished?.Invoke();
                Current = 0;
            }
        }

        private void OnEnergyIsOver()
        {
            if (_score.Level.Current >= Settings.CountToCompleted)
            {
                CompleteLevel();
            }
            else
            {
                Failed?.Invoke();
                Stop();
            }
        }
    }
}