﻿
using System;

namespace WoodFighter.Models
{
    public interface IDamageble<T>
    {
        public bool Dead { get; }
        public event Action<T> Died;
        public bool DealDamage();
    }
}