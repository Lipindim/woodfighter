﻿using UnityEngine;

namespace WoodFighter.Models
{
    [CreateAssetMenu(menuName = "Data/Upgrades/Speed", fileName = nameof(SpeedUpgrade))]
    public class SpeedUpgrade : Upgrade
    {
        [SerializeField] private float SpeedUpgradeValue = 1.0f;

        protected override void ActivateSingle(Player player)
        {
            player.UpgradeSpeed(SpeedUpgradeValue);
        }
    }
}
