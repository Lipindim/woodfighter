﻿using UnityEngine;

namespace WoodFighter.Modules
{
    [CreateAssetMenu(menuName = "Data/Upgrades/Settings", fileName = nameof(UpgradeSettings))]
    public class UpgradeSettings : ScriptableObject
    {
        [SerializeField] private string _name;
        [SerializeField] private string _description;
        [SerializeField] private Sprite _icon;
        [SerializeField] private int[] _prices;

        public string Name => _name;
        public string Description => _description;
        public Sprite Icon => _icon;
        public int[] Prices => _prices;
    }
}
