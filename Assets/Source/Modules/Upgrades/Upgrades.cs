﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace WoodFighter.Models
{
    public class Upgrades : IEnumerable<Upgrade>
    {
        public IEnumerable<Upgrade> _upgrades;

        public Upgrades(IEnumerable<Upgrade> upgrades)
        {
            _upgrades = upgrades;
        }

        public event Action Reseted;

        public IEnumerator<Upgrade> GetEnumerator()
        {
            return _upgrades.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _upgrades.GetEnumerator();
        }

        public IEnumerable<Upgrade> GetRandomUpgrades(int value)
        {
            return _upgrades
                .GetRandomElements(value);
        }

        public void Reset()
        {
            foreach (var upgrade in _upgrades)
                upgrade.Reset();
            Reseted?.Invoke();
        }
        
    }
}
