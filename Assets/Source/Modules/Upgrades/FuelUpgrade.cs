﻿using UnityEngine;

namespace WoodFighter.Models
{
    [CreateAssetMenu(menuName = "Data/Upgrades/Fuel", fileName = nameof(FuelUpgrade))]
    public class FuelUpgrade : Upgrade
    {
        [SerializeField] private int UpgradeEnergyValue = 10;

        protected override void ActivateSingle(Player player)
        {
            player.UpgradeEnergy(UpgradeEnergyValue);
        }
    }
}
