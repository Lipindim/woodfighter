﻿using UnityEngine;
using WoodFighter.Modules;

namespace WoodFighter.Models
{
    public abstract class Upgrade : ScriptableObject
    {
        [SerializeField] private UpgradeSettings _settings;

        public UpgradeSettings Settings => _settings;
        public bool CanUpgrade => UpgradeLevel < _settings.Prices.Length;
        public int UpgradeLevel { get; private set; }
        public int NextPrice => _settings.Prices[UpgradeLevel];

        public void Activate(Player player)
        {
            for (int i = 0; i < UpgradeLevel; i++)
                ActivateSingle(player);
        }

        protected abstract void ActivateSingle(Player player);

        public void IncreaseLevel()
        {
            UpgradeLevel++;
        }

        public void Reset()
        {
            UpgradeLevel = 0;
        }
    }
}
