﻿using UnityEngine;

namespace WoodFighter.Models
{
    [CreateAssetMenu(menuName = "Data/Upgrades/ExtraLive", fileName = nameof(ExtraLiveUpgrade))]
    public class ExtraLiveUpgrade : Upgrade
    {
        protected override void ActivateSingle(Player player)
        {
            player.AddExtraLive();
        }
    }
}

