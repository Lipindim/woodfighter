﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WoodFighter.Models
{
    public static class IEnumerableExtensions
    {
        private static Random _random = new Random();

        public static IEnumerable<T> GetRandomElements<T>(this IEnumerable<T> elements, int value)
        {
            List<T> elementsList = elements.ToList();
            List<T> randomElements = new();
            if (elementsList.Count <= value)
                return elementsList;

            for (int i = 0; i < value; i++)
            {
                int randomIndex = _random.Next(elementsList.Count);
                randomElements.Add(elementsList[randomIndex]);
                elementsList.RemoveAt(randomIndex);
            }

            return randomElements;
        }

        public static T GetRandomElement<T>(this IList<T> elements)
        {
            int randomIndex = _random.Next(elements.Count);
            return elements[randomIndex];
        }
    }
}
